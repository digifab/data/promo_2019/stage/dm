<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Generateur extends CI_Controller {		//referez vous au contrôleur Actualite pour les commentaires

	public function index($Univers='Main')
	{
		$this->load->helper('url');
		$this->load->model('modelDemonsMerveilles');		//Chargement du model
		$data['records'] = $this->modelDemonsMerveilles->nomUnivers();		// enregistrement de l'appel de la Methode modelGenerateur dans une variable tableau data [records]
		$title = $this->load->view('Univers/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$caroussel = $this->load->view('Univers/Caroussel',$data,true);
		$choixUnivers = $this->load->view('Univers/'.$Univers.'/Page1',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Univers/Template',['title' => $title,'head' => $head, 'navBar' => $navBar,'caroussel' => $caroussel,'choixUnivers' => $choixUnivers, 'footer' => $footer]);
	}

	
	public function AjoutPersonnage()		//?Peut étre mettre la Methode AjoutPersonnage dans un autre Controleur?
	{
		$this->load->helper('url');
		$Vue = $this->load->view('AjoutPersonnage/Ajout',[],true);
		$this->load->view('Template',['page' => $Vue,'title' => "DM - Generateur - Ajout de Personnage"]);
	}
}