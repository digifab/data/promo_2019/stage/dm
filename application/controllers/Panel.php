<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller{

    public function index()
    {
        $this->load->helper('url');		//chargement du helper url
		$this->load->model('modelDemonsMerveilles');
		$title = $this->load->view('Panel/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$sideNav = $this->load->view('Panel/SideNav',[],true);
		$actualite = $this->load->view('Panel/Main',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Panel/Template',['title' => $title,'head' => $head,'navBar' => $navBar, 'sideNav' => $sideNav ,'main' => $actualite,'footer' => $footer]);
    }

    public function sousCategorie()
    {
        $this->load->helper('url');		//chargement du helper url
		$this->load->model('modelDemonsMerveilles');
		$title = $this->load->view('Panel/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$sideNav = $this->load->view('Panel/SideNav',[],true);
		$actualite = $this->load->view('Panel/sousCategorie',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Panel/Template',['title' => $title,'head' => $head,'navBar' => $navBar, 'sideNav' => $sideNav ,'main' => $actualite,'footer' => $footer]);
	}

	public function sujet()
    {
        $this->load->helper('url');		//chargement du helper url
		$this->load->model('modelDemonsMerveilles');
		$title = $this->load->view('Panel/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$sideNav = $this->load->view('Panel/SideNav',[],true);
		$actualite = $this->load->view('Panel/sujet',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Panel/Template',['title' => $title,'head' => $head,'navBar' => $navBar, 'sideNav' => $sideNav ,'main' => $actualite,'footer' => $footer]);
	}
	
	public function message()
    {
        $this->load->helper('url');		//chargement du helper url
		$this->load->model('modelDemonsMerveilles');
		$title = $this->load->view('Panel/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$sideNav = $this->load->view('Panel/SideNav',[],true);
		$actualite = $this->load->view('Panel/message',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Panel/Template',['title' => $title,'head' => $head,'navBar' => $navBar, 'sideNav' => $sideNav ,'main' => $actualite,'footer' => $footer]);
	}
	
	public function sondage()
    {
        $this->load->helper('url');		//chargement du helper url
		$this->load->model('modelDemonsMerveilles');
		$title = $this->load->view('Panel/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$sideNav = $this->load->view('Panel/SideNav',[],true);
		$actualite = $this->load->view('Panel/sondage',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Panel/Template',['title' => $title,'head' => $head,'navBar' => $navBar, 'sideNav' => $sideNav ,'main' => $actualite,'footer' => $footer]);
	}
	
	public function event()
    {
        $this->load->helper('url');		//chargement du helper url
		$this->load->model('modelDemonsMerveilles');
		$title = $this->load->view('Panel/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$sideNav = $this->load->view('Panel/SideNav',[],true);
		$actualite = $this->load->view('Panel/event',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Panel/Template',['title' => $title,'head' => $head,'navBar' => $navBar, 'sideNav' => $sideNav ,'main' => $actualite,'footer' => $footer]);
    }

}