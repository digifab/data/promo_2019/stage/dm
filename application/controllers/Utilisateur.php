<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utilisateur extends CI_Controller
{
    public function __construct()       //si l'utilisateur n'est pas connecté il ne peut pas aller sur cette vue
    {
        parent::__construct();
       if(!isset($_SESSION['user_logged']))
       {
           $this->session->set_flashdata("error", "S'il vous plait connectez vous d'abord pour voir cette page");
            redirect("Auth/connexion");
       } 
    }
    
    public function profil()
    {
        $this->load->helper('url');
		$title = $this->load->view('Agenda/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$agenda = $this->load->view('Profil/Main',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Profil/Template',['title' => $title,'head' => $head,'navBar' => $navBar,'main' => $agenda,'footer' => $footer]);
    }
}