<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller {	//referez vous au contrôleur Actualite pour les commentaires

	public function index($year = NULL, $month = NULL)
	{
		$this->load->library('javascript');
		$this->load->helper('url');
		$this->load->model('modelDemonsMerveilles');
		$data['calender'] = $this->modelDemonsMerveilles->getcalender($year, $month);
		$title = $this->load->view('Agenda/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$main = $this->load->view('Agenda/Main',$data,true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Agenda/Template',['title' => $title,'head' => $head,'navBar' => $navBar,'main' => $main,'footer' => $footer]);
	}
}