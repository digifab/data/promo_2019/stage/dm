<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function connexion()
    {
        //vérification si les champs pseudo et mdp sont vides
        $this->form_validation->set_rules('pseudo','Pseudo','required');
        $this->form_validation->set_rules('mdp','Mot de passe','required|min_length[8]');
        //si il ne le sont pas
        if($this->form_validation->run() == true)
        {
            $pseudo = $_POST['pseudo'];
            $mdp = md5($_POST['mdp']);  //permet de desencrypter le mdp
            //on select toutes les colones de la table espace_membres 
           $this->db->select('*');
           $this->db->from('espace_membres');
           $this->db->where(array('pseudo' => $pseudo, 'mdp' => $mdp)); //dans un tableau on incrémente dans les varriables les valeurs des colonnes pseudo et mdp
           $query = $this->db->get();
           $user = $query->row();
		
           if($user != NULL)     //si le compte exsite
           {
            $this->session->set_flashdata("success","Vous êtes connecté");      //message d'indication
            $_SESSION['user_logged'] = true;        //utilisateur est connecté
            $_SESSION['pseudo'] = $user->pseudo;    //il est connecté par son pseudo
            redirect('Utilisateur/profil','refresh');
           }
           else
           {
                $this->session->set_flashdata("error","Le nom de compte ou le mot de passe exsiste déja");      //message d'erreur
                redirect('Auth/connexion','refresh');
           }
           if(!isset($_SESSION['user_logged']))
       {
           $this->session->set_flashdata("error", "S'il vous plait connectez vous d'abord pour voir cette page");
            redirect("Auth/connexion");
       } 
        }
        $this->load->helper('url');
		$title = $this->load->view('Agenda/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$agenda = $this->load->view('Connexion/Main',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Connexion/Template',['title' => $title,'head' => $head,'navBar' => $navBar,'main' => $agenda,'footer' => $footer]);
    }

    public function deconnexion()
    {
       unset($_SESSION);
       session_destroy();
       redirect("Auth/connexion","refresh"); 
    }
    
    public function inscription()
    {
        if (isset($_POST['forminscription']))
		{
            $this->form_validation->set_rules('pseudo','Pseudo','required|is_unique[espace_membres.pseudo]');       //le champ doit étre non vide, et unique par rapport a la colonne pseudo de la table espace_membres
            $this->form_validation->set_rules('e-mail','E-mail','required');    //non vide  
            $this->form_validation->set_rules('confE-mail','Confirmation E-mail','required|matches[e-mail]');       //non vide et etre identique avec le champ e-mail
            $this->form_validation->set_rules('mdp','Mot de passe','required|min_length[8]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W)/]');     //non vide avoir une longeur minimal de 8 et contenire 1 Minuscule 1 Majuscule 1 chiffre et un Carractère Spécial
            $this->form_validation->set_rules('confMdp','Confirmation Mot de passe','required|min_length[8]|matches[mdp]');     //non vide avoir une longeur minimal de 8 carractères et etre identique avec le champ mdp 

			if ($this->form_validation->run() == true) //si tout est bon
			{
                //dans un tableau on fait correspondre la valeur des champs avec le nom des colonnes de la table voulut
                $data = array(
                    'pseudo' => $_POST['pseudo'],
                    'mdp' => md5($_POST['mdp']),
                    'email' => $_POST['e-mail'],
                    'rang' => 0
                );
                $this->db->insert('espace_membres',$data);      //on insert le tableau dans la table espace_membres
                $this->session->set_flashdata("success","Votre compte a bien été enregistré");  //message d'info
                redirect("Auth/inscription","refresh");
			}
        }
        $this->load->helper('url');
		$title = $this->load->view('Agenda/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$agenda = $this->load->view('Inscription/Main',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Inscription/Template',['title' => $title,'head' => $head,'navBar' => $navBar,'main' => $agenda,'footer' => $footer]);
    }
}
