<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mention extends CI_Controller {	//Création de la class Actualité enfant de la classe CI_Controller qui charge la vue de l'actualité dans la template

	public function index()		//On charge les differents vues de la page dans des variables, puis on charge la template de la page voulut ainsi que les varibles du contrôleur dans les varaibles de la Template
	{
		$this->load->helper('url');		//chargement du helper url
		$this->load->model('modelDemonsMerveilles');
		$title = $this->load->view('MentionLegale/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$actualite = $this->load->view('MentionLegale/Main',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('NousContactez/Template',['title' => $title,'head' => $head,'navBar' => $navBar,'main' => $actualite,'footer' => $footer]);		// chargements des differentes vues dans la page Template
	}
}