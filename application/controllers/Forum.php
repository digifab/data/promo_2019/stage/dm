<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forum extends CI_Controller {		//referez vous au contrôleur Actualite pour les commentaires

	public function index($categorie= NULL ,$sousCategorieParent=NULL)
	{	$this->load->helper('url');
		$this->load->model('modelDemonsMerveilles');
		$data['affichage_categorie'] = $this->modelDemonsMerveilles->categorie();
		$data['affichage_souscategorie'] = $this->modelDemonsMerveilles->sous_categorie($categorie);
		$data['affichage_last_topic'] = $this->modelDemonsMerveilles->dernier_topic();
		$data['nbr_topic']=$this->modelDemonsMerveilles->nbr_topic($sousCategorieParent);
		$title = $this->load->view('Forum/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$forum = $this->load->view('Forum/Main',$data,true);
		$forumView = $this->load->view('Forum/ForumView',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Forum/Template',['title' => $title,'head' => $head,'navBar' => $navBar,'main' => $forum,'forumView' => $forumView,'footer' => $footer]);
	}

	public function aff_topic($categorie= NULL ,$sousCategorieParent=NULL)
	{
		$this->load->helper('url');	
		$this->load->model('modelDemonsMerveilles');
		$data['affichage_categorie'] = $this->modelDemonsMerveilles->categorie();
		$data['affichage_souscategorie'] = $this->modelDemonsMerveilles->sous_categorie($categorie);
		$data['affichage_last_topic'] = $this->modelDemonsMerveilles->dernier_topic();
		$data['nbr_topic']=$this->modelDemonsMerveilles->nbr_topic($sousCategorieParent);
		$title = $this->load->view('Forum/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$forum = $this->load->view('Forum/sousCategorie',$data,true);
		$forumView = $this->load->view('Forum/ForumView',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Forum/Template',['title' => $title,'head' => $head,'navBar' => $navBar,'main' => $forum,'forumView' => $forumView,'footer' => $footer]);
	}

	public function aff_message($categorie= NULL)
	{
		$this->load->helper('url');
		$this->load->model('modelDemonsMerveilles');
		$data['affichage_categorie'] = $this->modelDemonsMerveilles->categorie();
		$data['affichage_souscategorie'] = $this->modelDemonsMerveilles->sous_categorie($categorie);
		$data['affichage_last_topic'] = $this->modelDemonsMerveilles->dernier_topic();
		$title = $this->load->view('Forum/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$forum = $this->load->view('Forum/sujet',$data,true);
		$forumView = $this->load->view('Forum/ForumView',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Forum/Template',['title' => $title,'head' => $head,'navBar' => $navBar,'main' => $forum,'forumView' => $forumView,'footer' => $footer]);
	}

	public function sondage()
	{
		$this->load->helper('url');
		$this->load->model('modelDemonsMerveilles');
		$data['affichage_categorie'] = $this->modelDemonsMerveilles->categorie();
		$title = $this->load->view('Forum/Title',[],true);
		$head = $this->load->view('Shared/Head',[],true);
		$navBar = $this->load->view('Shared/NavBar',[],true);
		$sondage = $this->load->view('Forum/Sondage',$data,true);
		$forumView = $this->load->view('Forum/ForumView',[],true);
		$footer = $this->load->view('Shared/Footer',[],true);
		$this->load->view('Forum/Template',['title' => $title,'head' => $head,'navBar' => $navBar,'main' => $sondage,'forumView' => $forumView,'footer' => $footer]);
	}
}
