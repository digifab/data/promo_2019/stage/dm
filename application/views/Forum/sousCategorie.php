
<?php

$sousCat=$_GET["sousCat"];  // recuperation de la variable sousCat
//bdd = new PDO('mysql:host=localhost;dbname=demons_et_merveilles;charset=utf8' , 'root' , 'Dé&Me76620');
$req= $this->db->select("*")->from("f_topics")->where("sousCategorieParent", $sousCat)->get();
//$req="SELECT * FROM f_topics WHERE sousCategorieParent="".$sousCat.""";
if(!isset($_SESSION["user_logged"]))
{
}
else
    {
        $query = $this->db->select("idMembre")->from("espace_membres")->where("pseudo", $_SESSION["pseudo"])->get();
        foreach($query->result() as $r)
            {
                $idPosteur = $r->idMembre;
            }
    }

if (isset($_POST["formSujet"] ))
{
    $this->form_validation->set_rules("sujet","Sujet","required|regex_match[/^[a-z A-Z 0-9]/]");
    $this->form_validation->set_rules("contenu","Contenu","required|regex_match[/^[a-z A-Z 0-9]/]");
    if($this->form_validation->run() == TRUE)
    {
        $data=array(
            'idCreateur' => $idPosteur,
            'sujetTopics' =>$_POST['sujet'],
            'sousCategorieParent' => $sousCat,
            'contenuTopics' => $_POST['contenu']
        );
        $this->db->insert("f_topics",$data);
        $this->session->set_flashdata("success","Votre Sujet a bien été enregistré");  //message d'info
        redirect("Forum/aff_topic?sousCat=".$sousCat."?","refresh");
    }
}

?>


<!-- *********************************************************************************************************************************************************************************** -->
<!-- ***************************************************************************** Fil d'Arianne *************************************************************************************** -->
<!-- *********************************************************************************************************************************************************************************** -->

<div class="mt-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('Forum/')?>">Forum</a></li>
            <li class="breadcrumb-item active" aria-current="page"> <?=$sousCat?> </li>
        </ol>
    </nav>
</div>

<!-- *********************************************************************************************************************************************************************************** -->
<!-- ************************************************************************************** nouveau sujet ****************************************************************************** -->
<!-- *********************************************************************************************************************************************************************************** -->

<?php
        if (isset($_SESSION['success']))
        { 
?> <!-- affichage du message de réussite --> 
            <div class="alert alert-success"> <?= $_SESSION{'success'} ?></div>
<?php  
        }
        echo validation_errors('<div class="alert alert-danger">','</div>');
        if ($this->session->flashdata('error')) 
        { 
?> <!-- affichage des messages d'erreurs des champs invalide -->
            <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
<?php   }
if(!isset($_SESSION['user_logged']))
{
}
elseif($this->db->select('rang')->from('espace_membres')->where('pseudo', $_SESSION['pseudo'])->where('rang >= 1')->get()->result())
{?>
            <form method="POST">
                <div>
                    <p>
                        <a class="btn btn-primary" data-toggle="collapse" href="#creationSujet" role="button" aria-expanded="false" aria-controls="collapseExample">
                            Créer un nouveau sujet
                        </a>
                    </p>
                    <div class="collapse" id="creationSujet">
                        <div class="card card-body">

                            Entrez le nom du sujet: <input type="text" id="sujet" name="sujet"/>
                            Commencez la discution <input type="text" id="contenu" name="contenu"/>
                            <input type="submit" value="Créer" name="formSujet" />
                        </div>
                    </div>
                </div>
            </form>
            <?php
}?>
<!-- *********************************************************************************************************************************************************************************** -->
<!-- ******************************************************************* tableau d'affichage des sujets ******************************************************************************** -->
<!-- *********************************************************************************************************************************************************************************** -->

<div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>
                                            Sujet
                                        </th>
                                        <th>
                                            Date
                                        </th>
                                        <th>
                                            Contenu
                                        </th>
                                        <th>
                                            Nombre(s) de réponse(s)
                                        </th>

										<th>
											Dernier posteur
										</th>

										<th>
                                            Dernier Messages
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($req->result() as $row)
                                    {   
                                      
                                    ?>
                                    <tr>


                                        <td>
                                          <a href="<?= site_url('Forum/aff_message?sujet='.$row->sujetTopics.'&amp;sousCat='.$sousCat)?>">  <?=$row->sujetTopics?> </a>
                                        </td>


                                        <td>
                                            <?= $row->dateHeureCreation ?>
                                        </td>


                                        <td>
                                            <?= substr($row->contenuTopics ,0,30)."..." ?>
                                        </td>

                                        <td>
                                            <?php 
                                            echo $this->db->where("sujetTopics",$row->sujetTopics)->from("f_messages")->count_all_results();
                                            ?>
                                        </td>

										<td>
                                            <?php
                                            $reqlastposteur= $this->db->select("*")->from("f_messages")->where("sujetTopics", $row->sujetTopics)->where("sousCategorieParent",$sousCat)->order_by("dateHeurePost","DESC")->limit(1)->get();
                                            //$reqlastposteur= "SELECT * FROM f_messages WHERE sujetTopics='".$row["sujetTopics"]."'"."AND sousCategorieParent='".$sousCat."' ORDER BY dateHeurePost DESC LIMIT 1";
											foreach($reqlastposteur->result() as $lastman)
											{
                                                
                                                $reqIdMembre= $this->db->select("*")->from("espace_membres")->where("idMembre", $lastman->idMembre)->get();
												//$reqIdMembre="SELECT * FROM espace_membres WHERE idMembre='".$lastman['idMembre']."'";
												foreach($reqIdMembre->result() as $membres)
												{
												echo $membres->pseudo;
												}

											}
											?>
										</td>

                                        <td>
                                          <?php
                                            $reqlastmessage= $this->db->select("*")->from("f_messages")->where('sujetTopics',$row->sujetTopics)->where('sousCategorieParent', $sousCat)->order_by('dateHeurePost','DESC')->limit(1)->get() ;
                                            //$reqlastmessage="SELECT * FROM f_messages WHERE sujetTopics='".$row['sujetTopics']."'"."AND sousCategorieParent='".$sousCat."' ORDER BY dateHeurePost DESC LIMIT 1";
										  	foreach($reqlastmessage->result() as $lastm)
                                            {
										  		echo substr($lastm->contenuMessage,0,30)."...".'<br>';
											}
                                          ?>
                                        </td>

                                        
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
