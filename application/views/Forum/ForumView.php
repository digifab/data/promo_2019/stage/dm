<!-- LES COMMENTAIRES LOL -->

<!-- ********************************************************************************************************************************* -->
<!-- ********************************************* Création de l'espace Top Topic **************************************************** -->
<!-- ********************************************************************************************************************************* -->
<div class="mb-5 mt-5">

<table style="background: #e9ecef;" class="table table-sm">
		<thead class="text-center">	
			<tr>
			<th class="p-2">
				<img src="<?= base_url('assets/img/icone/png/new-file.png'); ?>" style="width: 32px; height: 32px; float: left;">
				Top Topic
			</th>
			</tr>
		</thead>
	<tbody>
					<?php
						$req = $this->db->select("sousCategorieParent, sujetTopics, COUNT(*) as nb ")->from("f_messages")->group_by("sujetTopics")->order_by("nb","DESC")->limit(5)->get();
						foreach ($req->result() as $row)
                        {
							?>
							<tr>
								<td class="p-2">
									<li class="pl-2" style="list-style: none; float: left;">
										<a style="color: #000; font-weight: bold;"  href="<?= site_url('Forum/aff_message?sujet='.$row->sujetTopics.'&amp;sousCat='.$row->sousCategorieParent)?>"> <!-- appel la fonction aff_message avec les variables sujet et sousCat -->
											<?= $row->sujetTopics ?>

										</a>

									</li>
									<img src="<?= base_url('assets/img/icone/png/next-page.png'); ?>" style="width: 25px; height: 25px; float: right; margin-right: 10px; ">
								</td>
							</tr>
							<?php
						}
					?>
	</tbody>
</table>


<!-- ********************************************************************************************************************************** -->
<!-- **************************************** Création de l'espace Derniers Message *************************************************** -->
<!-- ********************************************************************************************************************************** -->

<table style="background: #e9ecef;" class="table table-sm">
	<thead style="text-align: center; ">
		<tr>
			<th class="p-3">
				<img src="<?= base_url('assets/img/icone/png/open-book.png'); ?>" style="width: 32px; height: 32px; float: left;">
				Derniers messages 
			</th>
		</tr>
	</thead>
<tbody>

					<?php
						$req= $this->db->select("sousCategorieParent,sujetTopics,dateHeurePost")->from('f_messages')->group_by("sujetTopics")->order_by("dateHeurePost","DESC")->limit(5)->get();
						foreach($req->result() as $row)
						{
							$site='Forum/aff_message?sujet='.$row->sujetTopics.'&amp;sousCat='.$row->sousCategorieParent;
							?>
							<tr>
								<td class="p-2">
									<li class='pl-2' style=" float: left; list-style: none;">
									<a style="color: #2D2D2D;font-weight: bold;" href="<?= site_url($site)?>">
											<?= $row->sujetTopics; ?>
										</a>
									</li>
									<img src="<?= base_url('assets/img/icone/png/next-page.png'); ?>" style="width: 25px; height: 25px; float: right; margin-right: 10px; ">
								</td>
							</tr>
							<?php
						}
					?>
			</tbody>
		</table>

<!-- ********************************************************************************************************************************** -->
<!-- ********************************************************* Dernier Sondages ******************************************************* -->
<!-- ********************************************************************************************************************************** -->


<table style="background: #e9ecef;" class="table table-sm">
	<thead style="text-align: center; ">
		<tr>
			<th class="p-3">
				<img src="<?= base_url('assets/img/icone/png/open-book.png'); ?>" style="width: 32px; height: 32px; float: left;">
				Derniers Sondages 
			</th>
		</tr>
	</thead>
<tbody>

					<?php
						$req= $this->db->select(" titreSondage")->from("f_sondages")->order_by("idSondage","DESC")->limit(5)->get();
						foreach($req->result() as $row)
						{
							?>
							<tr>
								<td class="p-2">
									<li class='pl-2' style=" float: left; list-style: none;">
									<a style="color: #2D2D2D;font-weight: bold;" href="<?= site_url('Forum/sondage')?>">
											<?= $row->titreSondage; ?>
										</a>
									</li>
									<img src="<?= base_url('assets/img/icone/png/next-page.png'); ?>" style="width: 25px; height: 25px; float: right; margin-right: 10px; ">
								</td>
							</tr>
							<?php
						}
					?>
			</tbody>
		</table>

<!-- ********************************************************************************************************************************** -->
<!-- ********************************************************* Iframe Facebook ******************************************************** -->
<!-- ********************************************************************************************************************************** -->

<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fdetm76%2F&tabs=timeline&width=450&height=800&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="99%" height="800px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media">
</iframe>


</div>