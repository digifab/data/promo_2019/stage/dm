<meta charset="UTF-8">
<?php
  $sujet=$_GET['sujet'];  // recuperation de la variable sujet
  $sousCat=$_GET['sousCat'];  // recuperation de la variable sousCat
  $bdd = new PDO('mysql:host=localhost;dbname=demons_et_merveilles;charset=utf8' , 'root' , 'Dé&Me76620');
  $req=$this->db->select("*")->from("f_messages")->where("sujetTopics", $sujet)->where("sousCategorieParent", $sousCat)->order_by("idMessage")->get();
  $red=$this->db->select("*")->from("f_topics")->where("sousCategorieParent", $sousCat)->get();
  //$req = "SELECT * FROM f_messages WHERE sujetTopics='".$sujet."' AND sousCategorieParent='".$sousCat."' ORDER BY idMessage";  if (isset($_POST['formMessage'] ));
  
  if(!isset($_SESSION['user_logged']))
    {
    }
else
    {
        $query = $this->db->select('idMembre')->from('espace_membres')->where('pseudo', $_SESSION['pseudo'])->get();
  		foreach($query->result() as $r)
    	{
      		$idPosteur = $r->idMembre;
        }
    }
 	if(isset($_POST['formMessage']))
    {
   	    $this->form_validation->set_rules("post","Post",'required|regex_match[/^[a-z A-Z 0-9]/]');
	    if($this->form_validation->run() == TRUE)
	    {
            $data=array(
            'idMembre' => $idPosteur,
            'sujetTopics' => $sujet,
            'sousCategorieParent' => $sousCat,
            'contenuMessage' => $_POST['post']
            );
        $this->db->insert('f_messages',$data);
        $this->session->set_flashdata("success","Votre Message a bien été enregistré");  //message d'infos
        redirect("Forum/aff_message?sujet=$sujet &sousCat=$sousCat","refresh");
        }
        
    }

    foreach($req->result() as $row)
{
//***********************************************************************************************************************************************************************************
//***************************************************************************** SUPPRESSION ***************************************************************************************
// ***********************************************************************************************************************************************************************************
    $buttonDelete ='delete'.$row->idMessage;
    if(isset($_POST[$buttonDelete]))
        {
            $this->db->delete('f_messages',array('idMessage' => $row->idMessage));
			$this->session->set_flashdata("success","Votre Message a bien été Supprimé");  //message d'infos
			redirect("Forum/aff_message?sujet=$sujet &sousCat=$sousCat","refresh");
        }

//***********************************************************************************************************************************************************************************
//***************************************************************************** MODIFICATION ***************************************************************************************
// ***********************************************************************************************************************************************************************************
if(!isset($_SESSION['user_logged']))
{
}
else
{
$modif = $this->db->select('contenuMessage')->from('f_messages')->where('idMembre',$idPosteur)->where('idMessage',$row->idMessage)->get();
$buttonModif ='modification'.$row->idMessage;
if(isset($_POST[$buttonModif]))
    {
        $this->db->update('f_messages',array('contenuMessage' => $_POST['textModif']),array('idMessage' => $row->idMessage));
        $this->session->set_flashdata("success","Votre Message a bien été Modifier");  //message d'infos
		redirect("Forum/aff_message?sujet=$sujet &sousCat=$sousCat","refresh");
    }

}
//***********************************************************************************************************************************************************************************
//***************************************************************************** MODIFICATION TOPIC ***************************************************************************************
// ***********************************************************************************************************************************************************************************
}
foreach($red->result() as $row)
    {
        $buttonModifTopic = 'modificationTopic'.$row->idTopic;
        if(isset($_POST[$buttonModifTopic]))
        {
           $this->db->update('f_topics',array('contenuTopics' => $_POST['textModifTopic']),array('idTopic' => $row->idTopic));
           $this->session->set_flashdata("success","Le Topic a bien été Modifier");  //message d'infos
	    	redirect("Forum/aff_message?sujet=$sujet &sousCat=$sousCat","refresh");
        }
//***********************************************************************************************************************************************************************************
//***************************************************************************** SUPPRESSION TOPIC ***************************************************************************************
// ***********************************************************************************************************************************************************************************
        $buttonDeleteTopic ='deleteTopic'.$row->idTopic;
        if(isset($_POST[$buttonDeleteTopic]))
            {
                $this->db->delete('f_messages',array('sujetTopics' => $row->sujetTopics));
                $this->db->delete('f_topics',array('idTopic' => $row->idTopic));
                $this->session->set_flashdata("success","Le Topic a bien été Supprimé");  //message d'infos
		    	redirect("Forum/aff_topic?sousCat=$sousCat &?","refresh");
            }
    }
?>
<!-- *********************************************************************************************************************************************************************************** -->
<!-- ***************************************************************************** Fil d'Arianne *************************************************************************************** -->
<!-- *********************************************************************************************************************************************************************************** -->

<div class="mt-3">
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="<?= site_url('Forum/')?>">
                Forum
            </a>
        </li>
        <li name="sousCat" class="breadcrumb-item"> 
            <a href="<?=site_url('Forum/aff_topic?sousCat='.$sousCat)?>">
                <?= $sousCat ?>
            </a> 
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <?=$sujet?> 
        </li>
    </ol>
</nav>
</div>
<!-- *********************************************************************************************************************************************************************************** -->
<!-- ****************************************************************************************  head tab ******************************************************************************** -->
<!-- *********************************************************************************************************************************************************************************** -->
<?php
        if (isset($_SESSION['success']))
        { 
?> <!-- affichage du message de réussite --> 
            <div class="alert alert-success"> <?= $_SESSION{'success'} ?></div>
<?php  
        }
        echo validation_errors('<div class="alert alert-danger">','</div>');
        if ($this->session->flashdata('error')) 
        { 
?> <!-- affichage des messages d'erreurs des champs invalide -->
            <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
<?php   } ?>

<table class="table">
    <thead>
        <tr class="text-center">
            <th>
                Membre
            </th>

            <th>
                Date
            </th>

            <th width="60%">
                Contenu
            </th>
            <th>
                Modification / Suppresion
            </th>
        </tr>
    </thead>
    <tbody>

<!-- ****************************************************************************************************************************************************************************************** -->
<!-- ************************************************************************ Affichage du premier message et sujet du topic ****************************************************************** -->
<!-- ****************************************************************************************************************************************************************************************** -->


<?php
        $req1ermessage=$this->db->select("*")->from("f_topics")->where("sujetTopics", $sujet)->where("sousCategorieParent",$sousCat)->get();
        //$req1ermessage="SELECT * FROM f_topics WHERE sujetTopics='".$sujet."' AND sousCategorieParent='".$sousCat."'";
        foreach($req1ermessage->result() as $row)
        {
?>
            <tr class="text-center">
				<td>
                    <?php
                        $reqIdMembre=$this->db->select("*")->from("espace_membres")->where("idMembre",$row->idCreateur)->get();
						//$reqIdMembre="SELECT * FROM espace_membres WHERE idMembre='".$row['idCreateur']."'";
						foreach($reqIdMembre->result() as $membres)
						{
							echo $membres->pseudo;
						}
							?>
				</td>

				<td>
					<?= $row->dateHeureCreation ?>
				</td>

				<td>
					<?= $row->contenuTopics ?>
				</td>
                <td class="row">
                <?php
                    if(!isset($_SESSION['user_logged']))
                    {   
                    }
                    elseif($membres->pseudo == $_SESSION['pseudo'])
                    {?>
                    <button class="btn btn-warning" type="button" data-toggle="modal" data-target="#exampleModal1">modification</button>
                    <?php
                    foreach($req1ermessage->result() as $m)
                    {
                ?>
                  <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modification du message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method ="POST">
      <textarea type="text" name="textModifTopic" ><?= $m->contenuTopics; ?></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <input class="btn btn-warning" type="submit" name="modificationTopic<?= $row->idTopic ?>" value="Modification" />
        </form>
      </div>
    </div>
  </div>
</div>
                <?php
                 }

                }
                if(!isset($_SESSION['user_logged']))
                    {   
                    }
                    elseif($membres->pseudo == $_SESSION['pseudo'])
                    {?>
                    <form method ="POST">
                     <input class="btn btn-danger ml-3" type="submit" name="deleteTopic<?= $row->idTopic ?>" value="Supprimer Le Topic" />
                    </form>
                     <?php 
                    }
                ?>
                </td>
            </tr>
<?php
        }
?>
<!-- ****************************************************************************************************************************************************************************************** -->
<!-- ************************************************************************* Affichage des autres réponses ********************************************************************************** -->
<!-- ****************************************************************************************************************************************************************************************** -->
<?php
        foreach($req->result() as $row)
        {   
?>
            <tr class="text-center">
                <td>
                    <?php
                        $reqIdMembre=$this->db->select("*")->from("espace_membres")->where("idMembre",$row->idMembre)->get();
						//$reqIdMembre="SELECT * FROM espace_membres WHERE idMembre='".$row['idMembre']."'";
						foreach($reqIdMembre->result() as $membres)
						{
							echo $membres->pseudo;
						}
					?>
                 </td>

                <td>
                    <?= $row->dateHeurePost ?>
                </td>

                <td class="text-center">
                    <?= $row->contenuMessage ?>
                </td>
                <td class="row">
                <?php
                    if(!isset($_SESSION['user_logged']))
                    {   
                    }
                    elseif($membres->pseudo == $_SESSION['pseudo'])
                    {?>
                    <button class="btn btn-warning" type="button" data-toggle="modal" data-target="#exampleModal">modification</button>
                    <?php
                    foreach($modif->result() as $m)
                    {
                ?>
                  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modification du message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method ="POST">
      <textarea type="text" name="textModif" ><?= $m->contenuMessage; ?></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <input class="btn btn-warning" type="submit" name="modification<?= $row->idMessage ?>" value="Modification" />
        </form>
      </div>
    </div>
  </div>
</div>
                <?php
                 }

                }
                    if(!isset($_SESSION['user_logged']))
                    {   
                    }
                    elseif($membres->pseudo == $_SESSION['pseudo'])
                    {?>
                    <form method ="POST">
                     <input class="btn btn-danger ml-3" type="submit" name="delete<?= $row->idMessage ?>" value="Supprimer" />
                    </form>
                     <?php 
                    }
                ?>
                </td>
            </tr>
<?php
        }
?>
    </tbody>
</table>
     <?php
     if(!isset($_SESSION['user_logged']))
     {
         echo '<div class="alert alert-danger">Connectez vous pour pouvoir Poster</div>';
     }
     else
     {
     ?>
<form method="POST">
        <textarea type="text" id='post' name='post'class="ml-5" rows="12" style="width : 87%;"></textarea>
        <div class="text-left ml-5"><input type="submit" name="formMessage" class="btn btn-primary" style="width : 90%;"/></div>
        <?php
    }
?>
</form>