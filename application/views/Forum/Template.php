<!-- Referez-vous à la page Actualite/Template pour les commentaire -->
<!DOCTYPE html>
<html>
<head>
    <?= $head ?>
    <title><?= $title ?></title>
</head>
<body>
    <?= $navBar ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-9"><?= $main ?></div>
            <div class="col-md-3 text-right"><?= $forumView ?></div>
        </div>
    </div>   
    <?= $footer ?>
</body>
</html>