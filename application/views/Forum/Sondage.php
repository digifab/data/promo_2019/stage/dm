<?php
/**************************************************/
/*** Verification si l'utilisateur est connecté ***/
/**************************************************/
if(!isset($_SESSION['user_logged']))
{	
}
else
{
	$idPosteur = $this->db->select('idMembre')->from('espace_membres')->where('pseudo', $_SESSION['pseudo'])->get();
  		foreach($idPosteur->result() as $r)
    	{
      		$idPosteur = $r->idMembre;
		}
}	
/*********************************************************************************************************************************************************************************************************************
***************************************************************************************************** Recherche des $_POST *******************************************************************************************
**********************************************************************************************************************************************************************************************************************/


$nbChoix=0;
	if(isset($_POST['nbChoix']))
	{
		$nbChoix=$_POST['nbChoix'];
	}
	
$titre=0;
	if(isset($_POST['titreSondage']))
		{
			$titre=$_POST['titreSondage'];
		}

$datValidité=0;
	if(isset($_POST['datepicker']))
		{
			$datValidité=$_POST['datepicker'];
		}

/*********************************************************************************************************************************************************************************************************************
******************************************************************************************** Ajout d'un nouveau sondage dans la BDD **********************************************************************************
**********************************************************************************************************************************************************************************************************************/
if (isset($_POST['formSondage'] ))  // lors du clique sur le bouton formSondage
	{
		$this->form_validation->set_rules('titreSondage',"titre du sondage",'required'); //necessite 
		$this->form_validation->set_rules('datepicker',"Date",'required');
		if($this->form_validation->run() == TRUE)
		{
			$data=array(
				'idMembre'=> $idPosteur,
				'titreSondage' => $titre,
				'dateValiditeSondage' => $datValidité
			);
			$this->db->insert('f_sondages',$data);
		
			$j=0;
			while($j < $nbChoix)
			{	
				$this->form_validation->set_rules('choix'.$j,"Choix ".$j,'required');
				if($this->form_validation->run() == TRUE)
				{
					$numero="choix".$j;
					$choix = $_POST[$numero];
					$req2 = $this->db->select('idSondage')->from('f_sondages')->where('titreSondage',$titre)->get();
					foreach($req2->result() as $r2)
					{
						$idSondage=$r2->idSondage;
						$this->db->insert('f_s_choix',array('idSondage' => $idSondage,'choix' => $choix));
					}
					$j=$j+1;
				}
			}
			$this->session->set_flashdata("success","Votre Sondage a bien été enregistré");  //message d'infos
			redirect("Forum/sondage?","refresh");
		}
	}

/*********************************************************************************************************************************************************************************************************************
************************************************************************************* Ajout d'une nouvelle option d'une sondage a la bdd  ****************************************************************************
**********************************************************************************************************************************************************************************************************************/

	$query = $this->db->select('titreSondage, idSondage, idCalendar')->from('f_sondages')->get();
	$nbButton=1;
	$numRadio=1;
	$numSondage=1;
	foreach($query->result() as $q)
	{	
		$nameButton= "newOption".$q->idSondage;
		if(isset($_POST[$nameButton]))
		{
			$idSondagePourOption = $this->db->select('idSondage')->from('f_sondages')->where('titreSondage',$q->titreSondage)->get();
			foreach($idSondagePourOption->result() as $qIS)
			{
				$idSondageOption=$qIS->idSondage;
			}
			$newOption=$_POST[$nameButton];
			$this->form_validation->set_rules($nameButton, "Nouvelle Option",'required|is_unique[f_s_choix.choix]');
			if($this->form_validation->run() == true )
			{
				$this->db->insert( 'f_s_choix', array('idSondage'=> $idSondageOption,'choix' => $newOption ) );
				$this->db->delete('f_s_vote',array('idSondage' => $idSondageOption));
				$this->session->set_flashdata("success","Votre option a été ajouté aux sondage n°".$idSondageOption);  //message d'infos
				redirect("Forum/sondage?","refresh");
			}
		}
		$nbButton=$nbButton+1;

/* *********************************************************************************************************************************************************************************************************************
********************************************************************************************************** Vote ********************************************************************************************************
********************************************************************************************************************************************************************************************************************* */
if(isset($_POST['vote']))
{
	
	$idSondageVote=$q->idSondage;
	
	$nameRadio='radio'.$idSondageVote;
	if(isset($_POST[$nameRadio]))
	{	
		$idChoixVote=$_POST[$nameRadio];
		$verifVotant= $this->db->select('idSondage,idMembres')->from('f_s_vote')->where('idSondage',$idSondageVote)->where('idMembres',$idPosteur)->count_all_results();
		if($verifVotant == 0 )
		{
			$this->db->insert('f_s_vote', array('idSondage'=>$idSondageVote, 'idMembres' =>$idPosteur, 'idChoix'=> $idChoixVote));
			$this->session->set_flashdata("success","Merci d'avoir voté");  //message d'infos
			redirect("Forum/sondage?","refresh");
		}
		else
		{
			$this->session->set_flashdata("error","Vous avez déja voté pour ce sondage petit margoulin");  //message d'infos
			redirect("Forum/sondage?","refresh");
		}
			
	}
	
}
	
/* *********************************************************************************************************************************************************************************************************************
********************************************************************************************************** Changement d'avis *******************************************************************************************
********************************************************************************************************************************************************************************************************************* */
		$idSondageChange=$q->idSondage;
		$Change='changer'.$idSondageChange;
		if(isset($_POST[$Change]))
		{
			$this->db->delete('f_s_vote',array('idSondage' => $idSondageChange,'idMembres'=> $idPosteur));
			$this->session->set_flashdata("success","Vous pouvez revoter sur le Sondage n°".$numSondage);  //message d'infos
			redirect("Forum/sondage?","refresh");
		}
		$numSondage+=1;
/* *********************************************************************************************************************************************************************************************************************
********************************************************************************************************** Suppresion *******************************************************************************************
********************************************************************************************************************************************************************************************************************* */
$buttonDelete ='delete'.$q->idSondage;
if(isset($_POST[$buttonDelete]))
{
			$this->db->delete('f_sondages',array('idMembre' => $idPosteur,'idSondage' => $q->idSondage));
			$this->db->delete('f_s_choix',array('idSondage' => $q->idSondage));
			$this->db->delete('f_s_vote',array('idMembres'=> $idPosteur,'idSondage' => $q->idSondage));
			$this->db->delete('calendar',array('idMembre' => $idPosteur,'idCalendar' => $q->idCalendar));
			$this->session->set_flashdata("success","Votre sondage a bien été Supprimé");  //message d'infos
			redirect("Forum/sondage?","refresh");
}
}
?>
<!-- *********************************************************************************************************************************************************************************** -->
<!-- **************************************************************************************** Fil d'Arianne !!!!!!!!!!!! **************************************************************************** -->
<!-- *********************************************************************************************************************************************************************************** -->

<nav aria-label="breadcrumb" class="mt-3">
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?= site_url('Forum/')?>">Forum</a></li>
		<li class="breadcrumb-item active" aria-current="page"> Sondage </li>
	</ol>
</nav>


<!-- *********************************************************************************************************************************************************************************** -->
<!-- ****************************************************************************** Création Sondage *********************************************************************************** -->
<!-- *********************************************************************************************************************************************************************************** -->
<?php
if(!isset($_SESSION['user_logged']))
{
}
elseif($this->db->select('rang')->from('espace_membres')->where('pseudo', $_SESSION['pseudo'])->where('rang >= 1')->get()->result())
{
	?>
		<form method="POST">
	<?php
			if (isset($_SESSION['success']))
 				{
	?> <!-- affichage du message de réussite --> 
 					<div class="alert alert-success"> <?= $_SESSION{'success'} ?></div>
 	<?php 			
 				}
 				echo validation_errors('<div class="alert alert-danger">','</div>');
				if ($this->session->flashdata('error')) 
				{ 
	?> <!-- affichage des messages d'erreurs des champs invalide -->
 					<div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
	<?php 
				}
	?>
			<div class="text-left">
				<p>
					<a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
						Créer un nouveau sondage
					</a>
				</p>
				<div class="collapse" id="collapseExample">
					<div class="card card-body">
						<form method="POST" >
							<label>Combien de choix désirez-vous : </label>
							<input type="number" name="nbChoix" id="nbChoix" style="width:80px" >
							<input type="button"  value="Validez le nombre de choix" style=" width:80px" onclick="getValue()">	
							
							<div id="diffChoix" name="diffChoix" class="mt-1">

							</div>
							
							<label for="titreSondage">Titre :</label>
							<input name="titreSondage" type="text" style="width: 25%" placeholder="Indiquer le titre de votre sondage ">
							<label for="datepicker"> Date :</label>
							<input type="text" id="datepicker" name="datepicker" class="form-control datepicker" placeholder="sélectionnez une date" style=" width:25% ">

							<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
							<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
							<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
							<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
							<script>

								function getValue(){
									var choix = $("#nbChoix").val();
									var currentDiv = document.getElementById("diffChoix");
									$(currentDiv).empty();
									var i =0;
									while (i < choix)
									{
										var j =i+1;
										$(currentDiv).append(document.createTextNode("Choix"+j));
										$(currentDiv).append( $( "<input />").attr("name","choix"+i).attr("class","mt-1"));
										$(currentDiv).append(document.createElement( "br"));
										i=i+1;
										
									}
								}
								(function($){
									$.fn.datepicker.dates['fr'] = {
										days: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
										daysShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
										daysMin: [" Dimanche |", " Lundi |", " Mardi |", " Mercredi |", " Jeudi |", " Vendredi |", " Samedi |"],
										months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
										monthsShort: ["Janv. |", "Févr. |", "Mars |", "Avril |", "Mai |", "Juin |", "Juil. |", "Août |", "Sept. |", "Oct. |", "Nov. |", "Déc. |"],
										today: "Aujourd'hui",
										monthsTitle: "Mois",
										clear: "Effacer",
										weekStart: 1,
										format: "yyyy-mm-dd"
									};
								}(jQuery));

								$('.datepicker').datepicker({
									language: 'fr',
									autoclose: true,
									todayHighlight: true
								})
							</script>
							<br>
							<div class="text-right">
								<input  type="submit" value="Créer" name="formSondage">
							</div>
							<br>
						</form>	
					</div>
				</div>
			</div>
		</form>
	<?php
}
	?>
<!-- *********************************************************************************************************************************************************************************** -->
<!-- ***************************************************************************** Affichage Sondage *********************************************************************************** -->
<!-- *********************************************************************************************************************************************************************************** -->
<?php
$nbSondage= 1;
$query = $this->db->select('titreSondage,dateValiditeSondage,idSondage')->from('f_sondages')->order_by('dateCreationSondage','DESC')->get();
foreach($query->result() as $r)
{
?>
	<hr>
	<form method="POST">
		<?php
			echo("Sondage n°".$nbSondage);
			if(date('Y-m-d') < $r->dateValiditeSondage ) 
			{
		?>
				<input class="ml-5" type="text" name="newOption<?= $r->idSondage ?>" placeholder="Nouvelle option ?"> <input name="formNewOption<?= $nbSondage ?>" type="submit" value="Enregistrer">
		<?php
			}
			else
			{
				echo(" <strong> Le sondage est clôturé </strong>");
			}
			$queryIdSondage = $this->db->select('idSondage')->from('f_sondages')->where('titreSondage',$r->titreSondage)->get();
			foreach($queryIdSondage->result() as $qIS)//Affichage des sondages
			{
				$idSondage=$qIS->idSondage;
			}

			$queryChoice=$this->db->select('choix,idChoix')->from('f_s_choix')->where('idSondage',$idSondage)->order_by('idChoix' ,'ASC')->get();
			echo '<div class="my-3"><h4>'.$r->titreSondage.'<span class="pl-5">'.$r->dateValiditeSondage.'</h4></span></div>';
			$numeroChoix=1;
			foreach($queryChoice->result() as $rc) //Affichage des differentes proposition des sondages
			{
				$affPseudo = $this->db->select('pseudo')->from('espace_membres')->join('f_s_vote','espace_membres.idMembre = f_s_vote.idMembres')->where('idChoix',$rc->idChoix)->get();
		?>
				<div class="row">
					<div style=" margin-left : 1em;">
							<input type="radio" name="radio<?= $idSondage ?>" value="<?= $rc->idChoix ?>"> <br/> <!-- faire le name des boutton radio -->
					</div>
					<div class="col-11">
						<span class="badge badge-secondary">
							<h6>
								<label class="mr-5" style="float:left" for="<?= $idSondage ?>"><?= $rc->choix?></label>
								<span class="badge badge-light mt-1" data-toggle="tooltip" data-placement="right" title="<?php foreach($affPseudo->result() as $b){echo ($b->pseudo."\n");}?>" style="float:right"><?= $this->db->where('idSondage',$idSondage)->where('idChoix',$rc->idChoix)->from('f_s_vote')->count_all_results();?></span>
							</h6>
						</span>
					</div>	
				</div>
		<?php
				$numeroChoix+=1;
			}
		if(!isset($_SESSION['user_logged']))
		{
		}
		elseif($this->db->select('rang')->from('espace_membres')->where('pseudo', $_SESSION['pseudo'])->where('rang >= 1')->get()->result())
		{
		?>
			Votes total : <?= $this->db->where('idSondage',$idSondage)->from('f_s_vote')->count_all_results();?>
			<span class="ml-5">
				<?php
				if(date('Y-m-d') < $r->dateValiditeSondage )  // efface le bouton voter si le sondage est obsolète
				{?>
					<input type="submit" name="vote" value="Votez" >
			</span>
			<input type="submit" name="changer<?= $idSondage ?>" value="Changer d'avis">
			<?php
					$affichSuppr = $this->db->select('pseudo')->from('espace_membres')->join('f_sondages','espace_membres.idMembre = f_sondages.idMembre')->where('idSondage',$idSondage)->get();
					foreach($affichSuppr->result() as $suppr)
					{
						if($suppr->pseudo == $_SESSION['pseudo'])
						{?>
						 <input type="submit" name="delete<?= $r->idSondage ?>" value="Supprimer" >
						<?php 
						}
					}
				}
			}
			$nbSondage+=1;
			?>
	</form>	
<?php
}
?>
<hr/>