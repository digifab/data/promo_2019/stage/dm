<!-------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------->


<?php
$newSousCat="";

if (isset($_GET['cat']))
{
	$cat=$_GET['cat'];
}

if (isset($_POST['formSousCat'] ))
{
    $this->form_validation->set_rules('sousCat','Sous Catégorie','required|is_unique[f_souscategories.nomSousCategories]|regex_match[/^[a-z A-Z 0-9]/]');
    if($this->form_validation->run() == TRUE)
    {
        $data=array(
            'nomSousCategories' =>$_POST['sousCat'],
            'categorieParent' => $cat
        );
        $this->db->insert('f_souscategories',$data);
        $this->session->set_flashdata("success","Votre Sous Catégorie a bien été enregistré");  //message d'info
        redirect("Forum/index/$cat?Cat=$cat","refresh");
	}
}
?>


<!-------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------->


<div class="ml-3">
 	<nav aria-label="breadcrumb" class="mt-3">
	<ol class="breadcrumb ">
<?php
foreach($affichage_categorie as $affCat)
{
?>
	
		<li class="breadcrumb-item w-25"><a href="<?= site_url('Forum/index/'.$affCat->nomCategories.'?cat='.$affCat->nomCategories) ?>">
			<?= $affCat->nomCategories?>
		</a></li>
	

<?php
}
?>
	<li class="breadcrumb-item active" aria-current="page"><a href="<?= site_url('Forum/sondage') ?>">Sondage</a></li>
	</ol>
	</nav>
</div>


		

<!-------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------->


<?php
if(!isset($_SESSION['user_logged']))
{
}
elseif($this->db->select('rang')->from('espace_membres')->where('pseudo', $_SESSION['pseudo'])->where('rang >= 1')->get()->result())
{?>
<form method="POST">
	<div class="text-left mt-3">
	<?php if (isset($_SESSION['success'])) { ?> <!-- affichage du message de réussite --> 
		<div class="alert alert-success"> <?= $_SESSION{'success'} ?></div>
	<?php } ?>
	<?= validation_errors('<div class="alert alert-danger">','</div>'); ?>  <!-- affichage des messages d'erreurs des champs vides -->
	<?php if ($this->session->flashdata('error')) { ?> <!-- affichage des messages d'erreurs des champs invalide -->
		<div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
	<?php } ?>
		<p>
			<a class="btn btn-primary mt-2" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
				Créer une nouvelle sous-catégorie
			</a>
		</p>
		<div class="collapse" id="collapseExample">
			<div class="card card-body">

					Entrez le nom de la sous-catégorie : <input type="text" name="sousCat">

					<input type="submit" name="formSousCat" value="Créer" />

			</div>
		</div>
	</div>
</form>
	<?php
}?>
		<table class="table mt-3">
			<?php
			if($affichage_souscategorie == NULL)
			{
				echo "<h1>Choisissez une Catégorie</h1>";
			}
			else
			{?>
				<thead>
				<tr>
					<th>
						Sujet
					</th>
					<th>
						Date du dernier message (année/mois/jour)
					</th>
					<th>
						Nombre de topics
					</th>
					<th>
						Dernier sujet mis à jour
				</th>
					</tr>
			</thead>
			<tbody>
				<?php
			foreach($affichage_souscategorie as $affSousCat)
			{
				?>
				<tr>
					<td>
						<!-- Affichage nom catégorie -->
						<a name="sousCat"href="<?=site_url('Forum/aff_topic?sousCat='.$affSousCat->nomSousCategories)?>"><?= $affSousCat->nomSousCategories ?></a>
					</td>

					<td>
						<!-- Affichage date du dernier message -->
						<?php
						//$reqdatelastmessage='SELECT * FROM f_messages WHERE sousCategorieParent="'.$affSousCat->nomSousCategories.'" ORDER BY dateHeurePost LIMIT 1';
						$reqdatelastmessage = $this->db->select("dateHeurePost")->from("f_messages")->where("sousCategorieParent",$affSousCat->nomSousCategories)->order_by('dateHeurePost')->limit(1)->get();
						foreach($reqdatelastmessage->result() as $dateMessage)
						{
							echo $dateMessage->dateHeurePost;
						}
						?>
					</td>

					<td>
						<!-- Affichage nombres de topics -->
						<?php
						//$reqTopics='SELECT count(idTopic) FROM f_topics WHERE sousCategorieParent="'.$affSousCat->nomSousCategories.'"';
						$reqTopics=$this->db->select("COUNT(idTopic) as count")->from("f_topics")->where("sousCategorieParent",$affSousCat->nomSousCategories)->get();
						foreach( $reqTopics->result() as $nbTopic)
						{
							echo $nbTopic->count;
						}
						?>
					</td>

					<td>
						<!-- Affichage dernier topic maj -->
						<?php
							$reqlastmessage=$this->db->select('*')->from('f_messages')->where('sousCategorieParent',$affSousCat->nomSousCategories)->order_by("dateHeurePost")->limit(1)->get();
							//$reqlastmessage='SELECT * FROM f_messages WHERE sousCategorieParent="'.$affSousCat->nomSousCategories.'" ORDER BY dateHeurePost LIMIT 1';
							foreach($reqlastmessage->result() as $lastm)
							{
						?>
								<a href="<?= site_url('Forum/aff_message?sujet='.$lastm->sujetTopics.'&amp;sousCat='.$lastm->sousCategorieParent)?>"> <!-- appel la fonction aff_message avec les variables sujet et sousCat -->
									<?= $lastm->sujetTopics; ?>
								</a>
								<?php
							}
							?>
					</td>

				</tr>
				<?php
				}
			}
?>
			</tbody>
		</table>