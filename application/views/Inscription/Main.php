<!------------------------------------------------------------------------------------------------------------------------------------------->
<!----------------------------------------------------   Partie alerte php   ---------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------->


<div class="pt-5" style="min-height: 900px;">

<div class="mt-5 mx-auto w-50 h-80 p-5" style="box-shadow: 0px 0px 4px 0px #666;">
<form method="POST" action="">
	<div class='form-group'>
        <label for="pseudo">Pseudo :</label>
        <input type="text" class="form-control" placeholder="Votre pseudo" id="pseudo" name="pseudo" />
    </div>
    <div class='form-group'>
        <label for="e-mail">E-mail :</label>
        <input type="email" class="form-control" placeholder="Votre e-mail" id="e-mail" name="e-mail" />
    </div>
    <div class='form-group'>
        <label for="confEmail">Confirmer votre e-mail :</label>
        <input type="email" class="form-control" placeholder="Confirmez Votre e-mail" id="confE-mail" name="confE-mail" />
    </div>
    <div class='form-group'>
        <label for="mdp">Mot de passe :</label>
        <input type="password" class="form-control" placeholder="8 caractères | Majuscule | Minuscule | Chiffre |Caractere Speciaux"id="mdp" name="mdp" />
    </div>
    <div class='form-group'>
        <label for="confMdp">Confirmer votre Mot de passe :</label>
        <input type="password" class="form-control" placeholder="Confirmez votre mot de passe" id="confMdp" name="confMdp" />
    </div>
    <div class="mt-5" style="text-align: center;">
        <input type="submit" value="Créer votre compte" name="forminscription" class="btn btn-primary">
    </div>
</form>


</div>


<!------------------------------------------------------------------------------------------------------------------------------------------->
<!----------------------------------------------------   Partie alerte php   ---------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------->


<div class="mt-5 mx-auto w-50 pb-5">


<?php if (isset($_SESSION['success'])) { ?><!-- affichage du message de réussite -->
    <div class="alert alert-success"> <?= $_SESSION{'success'} ?></div>
<?php } ?>
<?= validation_errors('<div class="alert alert-danger">','</div>'); ?>  <!-- affichage des messages d'erreurs des champs vides -->
<?php if ($this->session->flashdata('error')) { ?>
    <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div><!-- affichage des messages d'erreurs des champs invalide -->
<?php } ?>  


</div>


<!------------------------------------------------------------------------------------------------------------------------------------------->
<!-----------------------------------------------------------   FIN   ----------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------->


</div>