<!-- Referez-vous à la page Actualite/Template pour les commentaire -->

<!DOCTYPE html>
<html>
<head>
    <?= $head ?>
    <title><?= $title ?></title>
</head>
<body>
    <?= $navBar ?>
    <div class="container-fluid"><?= $main ?></div>   
    <?= $footer ?>
</body>
</html>