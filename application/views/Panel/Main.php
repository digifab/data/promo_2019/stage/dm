<?php
	$req = $this->db->select("*")->from("espace_membres")->get();  // requete nécessaire

	//fonctionalité des bouttonS
	foreach($req->result() as $nomBoutton)
	{
		$bouttonSupr="supprimer".$nomBoutton->idMembre;
		$modifPseudo="modifPseudo".$nomBoutton->idMembre;
		$modifRang="modifRang".$nomBoutton->idMembre;
		
		//Activation boutton supprimer
		if(isset($_POST[$bouttonSupr]))
		{
			$ligneASupr = intval($nomBoutton->idMembre);
			$this->db->delete("espace_membres", array("idMembre" => $ligneASupr) ) ;
			echo '<script type="text/javascript">' . 'alert("Le compte a été supprimé");' . '</script>';
			redirect("Panel?","refresh");

			
		}

		//Activation du boutton modifier Pseudo
		if(isset($_POST[$modifPseudo]))
		{
			$newLogin = $_POST['pseudo'.$nomBoutton->idMembre];
			$data=array( 'pseudo' => $newLogin );
			$this->db->set($data)->where('idMembre',$nomBoutton->idMembre)->update('espace_membres');
			echo "<script type='text/javascript'>" . "alert('Le pseudo de l'utilisateur numéro ".$nomBoutton->idMembre."  a été modifié');" . "</script>";
			redirect("Panel?","refresh");
		}
		
		//Activation du boutton modifier Rang
		if(isset($_POST[$modifRang]))
		{
			$newRank= $_POST['rang'.$nomBoutton->idMembre];
			$data=array( 'rang' => $newRank );
			$this->db->set($data)->where('idMembre',$nomBoutton->idMembre)->update('espace_membres');
			echo "<script type='text/javascript'>" . "alert('Le rang de lutilisateur numéro ".$nomBoutton->idMembre."  a été modifié');" . "</script>";
			redirect("Panel?","refresh");
		}
		
	}

?>

<!-- ************************************************************************************************************************************************************************************************************
***************************************************************************************************** affichage du panel ****************************************************************************************
**************************************************************************************************************************************************************************************************************-->

<div>
	<form method = POST>
		<table style="background: #e9ecef;" class="table table-sm">
			<thead class="text-center">	
				<tr>
				<th class="p-2">
					Pseudo
				</th>
				<th class="p-2">
					email
				</th>
				<th class="p-2">
					password
				</th>
				<th class="p-2">
					Nom
				</th>
				<th class="p-2">
					Prenom
				</th>
				
				<th class="p-2">
					Rang
				</th>
				</tr>
			</thead>
		<tbody>
							<?php
								foreach ($req->result() as $row)
								{
									
									?>
									<tr>
										<td class="p-2 text-center">
											<input class="p-2" style="" type="text" name="pseudo<?= $row->idMembre ?>" value="<?= $row->pseudo; ?>">
											<br>
											<input name="modifPseudo<?= $row->idMembre ?>" type="submit" value="Modifier">
											<input name="supprimer<?= $row->idMembre ?>" type="submit" value="Suprimer">
										</td>

										<td class="p-2 text-center">
											<input class="p-2" style="" type="email" value="<?= $row->email; ?>">
											
										</td>

										<td class="p-2 text-center">
											<input class="p-2" style="" type="text" value="<?= $row->mdp;?>">
											
										</td>

										<td class="p-2 text-center">
											<input class="p-2" style="" type="text" value="<?= $row->nom; ?>">
										</td>
								
										<td class="p-2 text-center">
											<input class="p-2" style="" type="text" value="<?= $row->prenom; ?>">
										</td>

										<td class="p-2 text-center">
											<input class="p-2" style="" type="text" name="rang<?= $row->idMembre?>" value="<?= $row->rang; ?>">
											<br>
											<input name="modifRang<?= $row->idMembre ?>" type="submit" value="Modifier">
										</td>
									</tr>
									<?php
								}
							?>
			</tbody>
		</table>
	</form>
</div>