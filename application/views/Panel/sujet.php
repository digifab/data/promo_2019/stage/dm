<?php
	$req = $this->db->select("*")->from("f_topics")->get(); // requete de la page
	//fonctionalité des bouttonS
	foreach($req->result() as $nomBoutton)
	{
		$modifTopic="modifTopic".$nomBoutton->idTopic;
		$bouttonSupr="supprTopic".$nomBoutton->idTopic;
		$modifContenu="modifContent".$nomBoutton->idTopic;
		$modifSousCatParent="modifSousCat".$nomBoutton->idTopic;

		//Activation du boutton modifier le topic
		if(isset($_POST[$modifTopic]))
		{
			$newtopic=$_POST['topic'.$nomBoutton->idTopic];
			$this->db->set(array("sujetTopics"=>$newtopic))->where("sujetTopics",$nomBoutton->sujetTopics)->update("f_messages");
			$this->db->set(array("sujetTopics"=>$newtopic))->where("sujetTopics",$nomBoutton->sujetTopics)->update("f_topics");
			echo "<script type='text/javascript'>" . "alert('Le topic a été modifier');" . "</script>";
			redirect("Panel/sujet?","refresh");
		}
		
		//Activation boutton supprimer topic
		if(isset($_POST[$bouttonSupr]))
		{
			
			echo '<script type="text/javascript">' . 'alert("Le topic a été supprimé");' . '</script>';
			redirect("Panel/sujet?","refresh");
		}

		//Activation du boutton modifier contenu du topics
		if(isset($_POST[$modifContenu]))
		{
			$newContent=$_POST['contenuTopic'.$nomBoutton->idTopic];
			echo "<script type='text/javascript'>" . "alert('Le contenu du topic a été modifié');" . "</script>";
			redirect("Panel/sujet?","refresh");
		}

		//Activation du boutton modifier la sous categorie
		if(isset($_POST[$modifSousCatParent]))
		{
			$newSousCat=$_POST['sousCat'.$nomBoutton->idTopic];
			echo "<script type='text/javascript'>" . "alert('La sous-categorie a été modifié');" . "</script>";
			redirect("Panel/sujet?","refresh");
		}
		
		
	}

?>

<!-- ************************************************************************************************************************************************************************************************************
***************************************************************************************************** affichage du panel ****************************************************************************************
**************************************************************************************************************************************************************************************************************-->

<div>

	<form method = POST>
		<table style="background: #e9ecef;" class="table table-sm">
			<thead class="text-center">	
				<tr>
					<th class="p-2">
						Sujet Topic
					</th>

					<th class="p-2">
						Contenu topic
					</th>

					<th class="p-2">
						Sous-categorie Parent
					</th>
					
				</tr>
			</thead>
		<tbody>
							<?php
								foreach ($req->result() as $row)
								{
									
									?>
									<tr>
										<td class="p-2 text-center">
											<input class="p-2 mb-2" style="" type="text" value="<?= $row->sujetTopics ?>" name="topic<?= $row->idTopic ?>" >
											<br>
											<input name="modifTopic<?= $row->idTopic ?>" type="submit" value="Modifier">
											<input name="supprTopic<?= $row->idTopic ?>" type="submit" value="Suprimer">
										</td>

										<td class="p-2 text-center">
											<textarea class="p-2" style="width:80%;height:20px%" type="text" name="contenuTopics<?= $row->idTopic ?>"> <?= $row->contenuTopics ?> </textarea>
											<br>
											<input name="modifContent<?= $row->idTopic ?>" type="submit" value="Modifier">
										</td>

										<td class="p-2 text-center">
											<input class="p-2 mb-2" style="" type="text" value="<?= $row->sousCategorieParent?>" name="sousCat<?=$row->idTopic?>">
											<br>
											<input name="modifSousCat<?= $row->idTopic ?>" type="submit" value="Modifier">
										</td>

									
									</tr>
									<?php
								}
							?>
			</tbody>
		</table>
	</form>

		<?
	?>
</div>