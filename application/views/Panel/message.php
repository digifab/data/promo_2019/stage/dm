<?php
	$req = $this->db->select("*")->from("f_messages")->get();  // requete nécessaire

	//fonctionalité des bouttonS
	foreach($req->result() as $nomBoutton)
	{
		$bouttonSupr="supprimer".$nomBoutton->idMessage;
		$modifMembre="modifMembre".$nomBoutton->idMessage;
		$modifContenu="modifContenu".$nomBoutton->idMessage;
		
		//Activation boutton supprimer
		if(isset($_POST[$bouttonSupr]))
		{
			$ligneASupr = intval($nomBoutton->idMessage);
			$this->db->delete("f_messages", array("idMessage" => $ligneASupr) ) ;
			echo '<script type="text/javascript">' . 'alert("Le message a été supprimé");' . '</script>';
			redirect("Panel/message?","refresh");
		}

		//Activation du boutton modifier Pseudo
		if(isset($_POST[$modifMembre]))
		{
			$newLogin = $_POST['membreMessage'.$nomBoutton->idMessage];
			$data=array( 'idMembre' => $newLogin );
			$this->db->set($data)->where('idMessage',$nomBoutton->idMessage)->update('f_messages');
			echo "<script type='text/javascript'>" . "alert('L'utilisateur a été modifié');" . "</script>";
			redirect("Panel/message?","refresh");
		}
		
		//Activation du boutton modifier Rang
		if(isset($_POST[$modifContenu]))
		{
			$newContent = $_POST['contenu'.$nomBoutton->idMessage];
			$data=array( 'contenuMessage' => $newContent );
			$this->db->set($data)->where('idMessage',$nomBoutton->idMessage)->update('f_messages');
			echo "<script type='text/javascript'>" . "alert('Le message".$nomBoutton->idMessage." a été modifié');" . "</script>";
			redirect("Panel/message?","refresh");
		}
		
	}

?>

<!-- ************************************************************************************************************************************************************************************************************
***************************************************************************************************** affichage du panel ****************************************************************************************
**************************************************************************************************************************************************************************************************************-->

<div>
	<form method = POST>
		<table style="background: #e9ecef;" class="table table-sm">
			<thead class="text-center">	
				<tr>
					<th class="p-2">
						date
					</th>

					<th class="p-2">
						idMembre
					</th>
					
					<th class="p-2">
						Pseudo
					</th>

					<th>
						Topics
					</th>

					<th>
						Sous-Categorie
					</th>

					<th class="p-2" width="40%">
						Contenu
					</th>
				
				</tr>
			</thead>
			<tbody>
					<?php
						$bdd = NEW PDO('mysql:host=localhost;dbname=demons_et_merveilles', 'root', 'Dé&Me76620'); // à remplacer par du code igniter
						$req = $this->db->select('*')->from('f_messages')->order_by('dateHeurePost','DESC')->get();
						foreach ($req->result() as $row)
						{
					?>
							<tr>
								<td class="p-2 text-center">
									<input class="p-2"  style="" type="text" value="<?= $row->dateHeurePost; ?>">
									<input name="supprimer<?= $row->idMessage ?>" type="submit" value="Suprimer">
								</td>

								<td class="p-2 text-center">
									<input class="p-2" style="" name="membreMessage<?= $row->idMessage ?>" type="text" value="<?= $row->idMembre; ?>">
									<input name="modifMembre<?= $row->idMessage ?>" type="submit" value="Modifier">
								</td>

								<td class="p-2 text-center">
									<?php
										$pseudo= $this->db->select('pseudo')->from('espace_membres')->where('idMembre',$row->idMembre)->get();
										foreach($pseudo->result() as $jsp)
										{
									?>
											<input class="p-2" type="text" style=""  value="<?= $jsp->pseudo; ?>">
											<?php
											}
											?>
								</td>
										
								<td class="p-2 text-center">
									<input value="<?= $row->sujetTopics ?>">
								</td>
										
								<td class="p-2 text-center">
									<input value="<?= $row->sousCategorieParent ?>">
								</td>

								<td class="p-2 text-center">		
									<textarea  type="text" name="contenu<?= $row->idMessage ?>" style="width:80%" ><?= $row->contenuMessage?></textarea>
									<br>
									<input name="modifContenu<?= $row->idMessage ?>" type="submit" value="Modifier">
								</td>

										
							</tr>
					<?php
						}
					?>
			</tbody>
		</table>
	</form>
</div>