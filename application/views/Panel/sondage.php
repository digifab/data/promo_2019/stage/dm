<?php
	$req = $this->db->select("*")->from("f_sondages")->get();  // requete nécessaire

	//fonctionalité des bouttonS
	foreach($req->result() as $nomBoutton)
	{
        $suprSondage="suprSondage".$nomBoutton->idSondage;
        $modifSondage="modifTitreSondage".$nomBoutton->idSondage;
		$modifDate="modifDateSondage".$nomBoutton->idSondage;
        
		//**********************************
        //** Activation boutton supprimer **
        //**********************************
		if(isset($_POST[$suprSondage]))
		{
			$ligneASupr = intval($nomBoutton->idSondage);
            $this->db->delete("f_sondages", array("idSondage" => $ligneASupr) ) ;
            $this->db->delete("f_s_choix", array("idSondage"=> $ligneASupr));
            $this->db->delete("f_s_vote",array("idSondage"=> $ligneASupr));
			echo '<script type="text/javascript">' . 'alert("Le sondage a été supprimé");' . '</script>';
			redirect("Panel/sondage?","refresh");	
        }
        
        //*****************************************************
        //** Activation du boutton modifier titre du sondage **
        //*****************************************************
		if(isset($_POST[$modifSondage]))
		{
			$newTitle = $_POST['titreSondage'.$nomBoutton->idSondage];
			$data=array( 'titreSondage' => $newTitle );
			$this->db->set($data)->where('idSondage',$nomBoutton->idSondage)->update('f_sondages');
			echo "<script type='text/javascript'>" . "alert('Le titre du sondage ".$nomBoutton->idSondage."  a été modifié');" . "</script>";
			redirect("Panel/sondage?","refresh");
		}
        
        //*******************************************************************
        //** Activation du boutton modifier la date de validité du sondage **
        //*******************************************************************
		if(isset($_POST[$modifDate]))
		{
			$newDate= $_POST['dateSondage'.$nomBoutton->idSondage];
			$data=array( 'dateValiditeSondage' => $newDate );
			$this->db->set($data)->where('idSondage',$nomBoutton->idSondage)->update('f_sondages');
			echo "<script type='text/javascript'>" . "alert('La date de validité du sondage numéro ".$nomBoutton->idSondage."  a été modifié');" . "</script>";
			redirect("Panel/sondage?","refresh");
        }
        
        //Affectation du bouton par choix 
        $choix= $this->db->select('*')->from("f_s_choix")->where("idSondage",$nomBoutton->idSondage)->get();
        foreach($choix->result() as $c)
        {
            $modifChoix="modifChoix".$nomBoutton->idSondage.$c->idChoix;
            $suprChoix="suprChoix".$nomBoutton->idSondage.$c->idChoix;
            
            //activation du bouton modifier un choix
            if(isset($_POST[$modifChoix]))
            {
                $changeChoice=$_POST['choix'.$nomBoutton->idSondage.$c->idChoix];
                $data=array('choix'=> $changeChoice);
                $this->db->set($data)->where('idSondage',$nomBoutton->idSondage)->where('idChoix',$c->idChoix)->update('f_s_choix');
                echo "<script type='text/javascript'>" . "alert('Le choix ".$c->idChoix." du sondage numéro ".$nomBoutton->idSondage."  a été modifié');" . "</script>";
			    redirect("Panel/sondage?","refresh");
            }

            //activation du bouton supprimer un choix
            if(isset($_POST[$suprChoix]))
            {
                $delChoice=$c->idChoix;
                $this->db->delete('f_s_choix',array( 'idChoix' => $delChoice ));
                $this->db->delete('f_s_vote', array('idChoix'=> $delChoice));
                echo "<script type='text/javascript'>" . "alert('Le choix du sondage numéro ".$nomBoutton->idSondage."  a été supprimer');" . "</script>";
                echo "<script type='text/javascript'>" . "alert('Le choix du sondage numéro ".$nomBoutton->idSondage."  a été supprimer');" . "</script>";

                redirect("Panel/sondage?","refresh");
            }
        }
		
	}

?>

<!-- ************************************************************************************************************************************************************************************************************
***************************************************************************************************** affichage du panel ****************************************************************************************
**************************************************************************************************************************************************************************************************************-->
<div>

<form method = POST>
	<table style="background: #e9ecef;" class="table table-sm">
		<thead class="text-center">	
			<tr>
                <th class="p-2">
                    Titre
                </th>

                <th class="p-2">
                    Date de Création
                </th>

                <th class="p-2">
                    Date de Validité
                </th>

                <th class="p-2">
                    Choix
                </th>
			</tr>
		</thead>
	    <tbody>
            
				<?php
					$req = $this->db->select("*")->from("f_sondages")->order_by('dateCreationSondage','DESC')->get();
					foreach ($req->result() as $row)
					{ 
			    ?>
					<tr>			
                        <td>
                            <textarea  name="titreSondage<?= $row->idSondage ?>"  style="width:100%;height:10%"> <?= $row->titreSondage ?> </textarea>
                            <br>
                            <input type="submit" value="modifier" name="modifTitreSondage<?= $row->idSondage ?>">
                            <input type="submit" value="supprimer" name="suprSondage<?= $row->idSondage ?>">
                        </td>

                        <td class="text-center">
                            <input type="text" value="<?= $row->dateCreationSondage ?>">
                        </td>

                        <td class="text-center">
                            <input type="text" value="<?= $row->dateValiditeSondage ?>" name="dateSondage<?= $row->idSondage ?>">
                            <input type="submit" value="modifier" name="modifDateSondage<?= $row->idSondage ?>">
                        </td>

                        <td class="text-center">
                            <?php
                                $choix= $this->db->select('*')->from("f_s_choix")->where("idSondage",$row->idSondage)->get();
                                foreach($choix->result() as $c)
                                {
                            ?>
                                    <input type="text" value="<?= $c->choix ?>" name="choix<?= $row->idSondage ?><?= $c->idChoix ?>" style="width:60%">
                                    <input type="submit" value="modifier" name="modifChoix<?= $row->idSondage ?><?= $c->idChoix ?>">
                                    <input type="submit" value="supprimer" name="suprChoix<?= $row->idSondage ?><?= $c->idChoix ?>">
                                    <br>                    
                            <?php
                            }
                            ?>
                            <br>
                        </td>
                    <tr>
                        
				<?php
				    }
                ?>
            
		</tbody>
	</table>
</form>

	<?
?>
</div>