<?php
		
	$req = $this->db->select("*")->from("f_souscategories")->get();
	//fonctionalité des bouttonS
	foreach($req->result() as $nomBoutton)
	{
		$nomSousCat="modifNomSousCat".$nomBoutton->idSousCategories;
		$bouttonSupr="supprSouCat".$nomBoutton->idSousCategories;
		$modifCatParent="modifCatParent".$nomBoutton->idSousCategories;
		
		
		//Activation boutton supprimer
		if(isset($_POST[$bouttonSupr]))
		{
			$ligneASupr = intval($nomBoutton->idSousCategories);
			$this->db->delete("f_messages",array("sousCategorieParent" => $nomBoutton->nomSousCategories));
			$this->db->delete("f_topics",array("sousCategorieParent"=>$nomBoutton->nomSousCategories));
			$this->db->delete("f_souscategories", array("idSousCategories" => $ligneASupr) ) ;
			echo '<script type="text/javascript">' . 'alert("La sous-catégorie a été supprimé");' . '</script>';
			redirect("Panel/sousCategorie","refresh");
		}

		//Activation du boutton modifier Pseudo
		if(isset($_POST[$nomSousCat]))
		{
			$newName = $_POST['nomSousCat'.$nomBoutton->idSousCategories];
			$data=array( 'nomSousCategories' => $newName );
			$this->db->set(array('sousCategorieParent' => $newName))->where('sousCategorieParent',$nomBoutton->nomSousCategories)->update('f_messages');
			$this->db->set(array('sousCategorieParent'=> $newName))->where('sousCategorieParent',$nomBoutton->nomSousCategories)->update('f_topics');
			$this->db->set($data)->where('idSousCategories',$nomBoutton->idSousCategories)->update('f_souscategories');
			echo "<script type='text/javascript'>" . "alert('Le nom de la sous-catégorie a été changé');" . "</script>";
			redirect("Panel/sousCategorie","refresh");
		}
		
		//Activation du boutton modifier Rang
		if(isset($_POST[$modifCatParent]))
		{
			$newCatParent = $_POST['catParent'.$nomBoutton->idSousCategories];
			$data=array( 'categorieParent' => $newCatParent);
			$this->db->set($data)->where('idSousCategories',$nomBoutton->idSousCategories)->update('f_souscategories');
			echo "<script type='text/javascript'>" . "alert('La categorie parent a été modifié');" . "</script>";
			redirect("Panel/sousCategorie","refresh");
		}
	
}
?>


<!-- ************************************************************************************************************************************************************************************************************
***************************************************************************************************** affichage du panel ****************************************************************************************
**************************************************************************************************************************************************************************************************************-->
<div>

<form method = POST>
	<table style="background: #e9ecef;" class="table table-sm">
		<thead class="text-center">	
			<tr>
                <th class="p-2">
                    Nom sous categorie
                </th>

                <th class="p-2">
                    Categorie parent
                </th>

                <th class="p-2">
                    Date de création
                </th>

			</tr>
		</thead>
	<tbody>
						<?php
							foreach ($req->result() as $row)
							{
								?>
								<tr>
									<td class="p-2 text-center">
										<input class="p-2" style="" type="text" name="nomSousCat<?= $row->idSousCategories ?>" value="<?= $row->nomSousCategories; ?>">
										<br>
										<input name="modifNomSousCat<?= $row->idSousCategories ?>" type="submit" value="Modifier">
										<input name="supprSouCat<?= $row->idSousCategories ?>" type="submit" value="Suprimer">
									</td>
										
									<td class="p-2 text-center">
										<input class="p-2" style="" type="text" name="catParent<?= $row->idSousCategories ?>" value="<?= $row->categorieParent; ?>">
										<input name="modifCatParent<?= $row->idSousCategories ?>" type="submit" value="Modifier">
									</td>

									<td class="p-2 text-center">
										<input class="p-2" style="" type="text" value="<?= $row->dateHeureCreation; ?>">
										
									</td>
								</tr>
								<?php
													
							}
						?>
		</tbody>
	</table>
</form>

	<?
?>
</div>