<!----------------------------------------------------------------------------------------------->
<!------------------------------------------   MAIN   ------------------------------------------->
<!----------------------------------------------------------------------------------------------->
	<div class="w-100" style="box-shadow: 2px 0px 2px 0px #999; background: grey; height: 100%;">

		<div class="p-2 m-0 text-center text" style=" height: 80px;">
			<li style="list-style: none;">
			<a href="<?= site_url('Panel/'); ?>" style="color: #DEDEDE; text-decoration: none; display: inline-block;">Gestion des Membres</a>
			</li>
		</div>

		<hr><hr>

		<div class="p-1 m-0 text-center" style=" height: 80px;">
			<li style="list-style: none;">
				<a href="<?= site_url('Panel/sousCategorie'); ?>" style=" color: #DEDEDE; text-decoration: none; display: inline-block;">Gestion des sous-Categorie</a>
			</li>
		</div>

		<div class="p-2 m-0 text-center" style=" height: 80px;">
			<li style="list-style: none;">
			<a href="<?= site_url('Panel/sujet'); ?>" style="color: #DEDEDE; text-decoration: none; display: inline-block;">Gestion des sujets</a>
			</li>
		</div>

		<div class="p-2 m-0 text-center" style=" height: 80px">
			<li style="list-style: none;">
			<a href="<?= site_url('Panel/message'); ?>" style="color: #DEDEDE; text-decoration: none; display: inline-block;">Gestion des Posts</a>
			</li>
		</div>

		

		<hr><hr>

		<div class="p-2 m-0 text-center" style=" height: 80px;">
			<li style="list-style: none;">
			<a href="<?= site_url('Panel/sondage'); ?>" style="color: #DEDEDE; text-decoration: none; display: inline-block;">Gestion des Sondages</a>
			</li>
		</div>
			
	</div>
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->