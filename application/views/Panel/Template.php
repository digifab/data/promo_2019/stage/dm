<!DOCTYPE html>
<html>
<head>
    <?= $head ?>
    <title><?= $title ?></title>
</head>
<body>
    <?= $navBar ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-2">
                <?= $sideNav ?>    
            </div>
            <div class="col-10">
                <?= $main ?>
            </div>
        </div>
    </div>   
    <?= $footer ?>
</body>
</html>