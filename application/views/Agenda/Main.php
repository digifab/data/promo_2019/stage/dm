
	<div class="col-6" id="body"><?= $calender?></div>
	<div class="col-3">
	
	<?php
if(!isset($_SESSION['user_logged']))
{	
}
else
{
	$idPosteur = $this->db->select('idMembre')->from('espace_membres')->where('pseudo', $_SESSION['pseudo'])->get();
  		foreach($idPosteur->result() as $r)
    	{
      		$idPosteur = $r->idMembre;
		}
}	
/*********************************************************************************************************************************************************************************************************************
***************************************************************************************************** Recherche des $_POST *******************************************************************************************
**********************************************************************************************************************************************************************************************************************/
if(isset($_POST['formSondage']) || isset($_POST['formEvent']) )
        {
            $placeSondage = $_POST['placeSondage'];
            $datValidité = $_POST['datepicker'];
			$titre = $_POST['titreSondage'];
			$dateEvent =$_POST['dateEvent'];
			$hourEvent = $_POST['hourEvent'];
        }
$nbChoix = null;
if(isset($_POST['nbChoix']))
        {
            $nbChoix = $_POST['nbChoix'];
        }
/*********************************************************************************************************************************************************************************************************************
******************************************************************************************** Ajout d'une nouvelle Partie dans la BDD **********************************************************************************
**********************************************************************************************************************************************************************************************************************/
if (isset($_POST['formSondage'] ))
	{
		$this->form_validation->set_rules('titreSondage',"titre du sondage",'required');
		$this->form_validation->set_rules('datepicker',"Date",'required');
		$this->form_validation->set_rules('hourEvent',"Heure",'required');
		$this->form_validation->set_rules('dateEvent',"Date de l'evenement",'required');
		$this->form_validation->set_rules('placeSondage',"l'emplacement",'required');
		if($this->form_validation->run() == TRUE)
		{
			$this->db->insert('calendar',array('dateEvent' => $dateEvent,'hourEvent' => $hourEvent.'00' ,'content' => "<h5>Sondage :</h5> \n".$titre." \n".$placeSondage,'idMembre' => $idPosteur));
			
			foreach($this->db->select('idCalendar')->from('calendar')->limit(1)->order_by('idCalendar','DESC')->get()->result() as $g)
			{
				$idCalendar = $g->idCalendar;
			}
			$data=array(
				'idMembre'=> $idPosteur,
				'titreSondage' => $titre." ".$placeSondage,
				'dateValiditeSondage' => $datValidité,
				'idCalendar' => $idCalendar
			);
			$this->db->insert('f_sondages',$data);
            $req2 = $this->db->select('idSondage')->from('f_sondages')->where('titreSondage',$titre." ".$placeSondage)->get();
            foreach($req2->result() as $r2)
					{
                        $this->db->insert('f_s_choix',array('idSondage' => $r2->idSondage,'choix' => "Je participe"));
						$this->db->insert('f_s_choix',array('idSondage' => $r2->idSondage,'choix' => "Je ne participe pas"));
						
					}
			$j=0;
			while($j < $nbChoix)
			{	
				$this->form_validation->set_rules('choix'.$j,"Choix ".$j,'required');
				if($this->form_validation->run() == TRUE)
				{
					$numero="choix".$j;
					$choix = $_POST[$numero];
					$req2 = $this->db->select('idSondage')->from('f_sondages')->where('titreSondage',$titre." ".$placeSondage)->get();
					foreach($req2->result() as $r2)
					{
                        $this->db->insert('f_s_choix',array('idSondage' => $r2->idSondage,'choix' => $choix));
                        $this->db->insert('f_s_choix',array('idSondage' => $r2->idSondage,'choix' => "J'équilibre"));
					}
					$j=$j+1;
				}
			}
			$this->session->set_flashdata("success","Votre Sondage a bien été enregistré");  //message d'infos
			redirect("Forum/sondage?","refresh");
		}
	}





	if (isset($_POST['formEvent'] ))
	{
		$this->form_validation->set_rules('titreSondage',"titre du sondage",'required');
		$this->form_validation->set_rules('hourEvent',"Heure",'required');
		$this->form_validation->set_rules('dateEvent',"Date de l'evenement",'required');
		$this->form_validation->set_rules('placeSondage',"l'emplacement",'required');
		if($this->form_validation->run() == TRUE)
		{
			$this->db->insert('calendar',array('dateEvent' => $dateEvent,'hourEvent' => $hourEvent.'00','content' => "<h5>Evenement : </h5>\n".$titre." \n".$placeSondage,'idMembre' => $idPosteur));
			$this->session->set_flashdata("success","Votre Evenement  a bien été enregistré");  //message d'infos
			redirect("Agenda?");
		}
	}
	
/*********************************************************************************************************************************************************************************************************************
******************************************************************************************** Formulaire de la nouvelle Partie**********************************************************************************
**********************************************************************************************************************************************************************************************************************/
?>
<?php
if(!isset($_SESSION['user_logged']))
{
}
elseif($this->db->select('rang')->from('espace_membres')->where('pseudo', $_SESSION['pseudo'])->where('rang >= 1')->get()->result())
{
	?>
		<form method="POST">
			<div class="text-left">
				<p>
					<a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
						Prévoire une Partie
					</a>
				</p>
				<?php
			if (isset($_SESSION['success']))
 				{
	?> <!-- affichage du message de réussite --> 
 					<div class="alert alert-success"> <?= $_SESSION{'success'} ?></div>
 	<?php 			
 				}
 				echo validation_errors('<div class="alert alert-danger">','</div>');
				if ($this->session->flashdata('error')) 
				{ 
	?> <!-- affichage des messages d'erreurs des champs invalide -->
 					<div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
	<?php 
				}
	?>
				<div class="collapse" id="collapseExample">
					<div class="card card-body">
						<form method="POST" >
                            <label for="titreSondage">Titre de la partie:</label>
							<input name="titreSondage" type="text" placeholder="Indiquer le titre">
							<label>indiquez le nombre d'autres parties : </label>
							<input type="number" name="nbChoix" id="nbChoix"/>
							<input type="button" value="Validation choix" style="width:42%" onclick="getValue()"/>	
							
							<div id="diffChoix" name="diffChoix" class="mt-1">

							</div>
                            
							<label for="placeSondage">l'emplacement :</label>
							<input name="placeSondage" type="text" placeholder="Indiquer l'emplacement de la Partie">
							<label for="datepicker"> Date de fin du sondage :</label>
							<input type="text" id="datepicker" name="datepicker" class="form-control datepicker" placeholder="sélectionnez une date" style=" width:50% ">
							<label for="dateEvent"> la date et heure de la partie :</label>
							<div class="row">
								<input type="text" id="dateEvent" name="dateEvent" class="form-control datepicker" placeholder="Date" style=" width:50% ">
								<input type="text" id="hourEvent" name="hourEvent" placeholder="Heure format : Hm" style=" width:50% ">
								</div>        
							</div>
							<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
							<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
							<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
							<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
							<script>
							function getValue(){
									var choix = $("#nbChoix").val();
									var currentDiv = document.getElementById("diffChoix");
									$(currentDiv).empty();
									var i =0;
									while (i < choix)
									{
										var j =i+1;
										$(currentDiv).append(document.createTextNode("Choix"+j));
										$(currentDiv).append( $( "<input />").attr("name","choix"+i).attr("class","mt-1"));
										$(currentDiv).append(document.createElement( "br"));
										i=i+1;
										
									}
								}
							
								(function($){
									$.fn.datepicker.dates['fr'] = {
										days: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
										daysShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
										daysMin: ["Dimanche|", "Lundi|", "Mardi|", "Mercredi|", "Jeudi|", "Vendredi|", "Samedi|"],
										months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
										monthsShort: ["Janv. |", "Févr. |", "Mars |", "Avril |", "Mai |", "Juin |", "Juil. |", "Août |", "Sept. |", "Oct. |", "Nov. |", "Déc. |"],
										today: "Aujourd'hui",
										monthsTitle: "Mois",
										clear: "Effacer",
										weekStart: 1,
										format: "yyyy-mm-dd"
									};
								}(jQuery));
								$('.datepicker').datepicker({
									language: 'fr',
									autoclose: true,
									todayHighlight: true
								})
							</script>
							<br>
							<div class="text-right">
								<input class="btn btn-primary" type="submit" value="Créer" name="formSondage">
							</div>
							<br>
						</form>	
					</div>
					</div>
				</div>
				<div class="col-3">
	<div class="text-left">
				<p>
					<a class="btn btn-primary" data-toggle="collapse" href="#collapseEvent" role="button" aria-expanded="false" aria-controls="collapseExample">
						Prévoire un un Evenement
					</a>
				</p>
				<div class="collapse" id="collapseEvent">
					<div class="card card-body">
						<form method="POST" >
                            <label for="titreSondage">Titre de l'evenement :</label>
							<input name="titreSondage" type="text" placeholder="Indiquer le titre">
							<label for="placeSondage">l'emplacement :</label>
							<input name="placeSondage" type="text" placeholder="Indiquer l'emplacement de la Partie">
							<label for="dateEvent"> la date et heure de l'evenement :</label>
							<div class="row">
								<input type="text" id="dateEvent" name="dateEvent" class="form-control datepicker" placeholder="Date" style=" width:50% ">
								<input type="text" id="hourEvent" name="hourEvent" placeholder="Heure format : Hm" style=" width:50% ">
								</div>        
							</div>
							<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
							<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
							<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
							<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
							<script>
								(function($){
									$.fn.datepicker.dates['fr'] = {
										days: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
										daysShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
										daysMin: ["Dimanche|", "Lundi|", "Mardi|", "Mercredi|", "Jeudi|", "Vendredi|", "Samedi|"],
										months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
										monthsShort: ["Janv. |", "Févr. |", "Mars |", "Avril |", "Mai |", "Juin |", "Juil. |", "Août |", "Sept. |", "Oct. |", "Nov. |", "Déc. |"],
										today: "Aujourd'hui",
										monthsTitle: "Mois",
										clear: "Effacer",
										weekStart: 1,
										format: "yyyy-mm-dd"
									};
								}(jQuery));
								$('.datepicker').datepicker({
									language: 'fr',
									autoclose: true,
									todayHighlight: true
								})
							</script>
							<br>
							<div class="text-right">
								<input class="btn btn-primary" type="submit" value="Créer" name="formEvent">
							</div>
							<br>
						</form>	
	</div>	
			</div>
		</form>
	<?php
}
	?>
</div>