<!-- Referez-vous à la page Actualite/Template pour les commentaire -->

<!DOCTYPE html>
<html>
<head>
    <?= $head ?>
    <title><?= $title ?></title>
</head>
<style type="text/css">
	table
    {
        border: 15px solid #c10000;
        border-collapse:collapse;
    }

    td
    {
        width: 70px;
        height: 70px;
        text-align: center;
        border: 1px solid #e2e0e0;
        font-size: 18px;
        font-weight: bold;
    }

    th
    {
        height: 50px;
        padding-bottom: 8px;
        background:#c10000;
        font-size: 20px;
    }

    .prev_sign a, .next_sign a
    {
        color:black;
        text-decoration: none;
    }

    tr.week_name
    {
        font-size: 16px;
        font-weight:400;
        color:#c10000;
        width: 10px;
        background-color: #efe8e8;
    }

    .highlight
    {
        background-color:#fe5252;
        color:white;
        height: 100%;
        padding-top: 13px;
        padding-bottom: 7px;
    }
    
    .calender .days td
    {
        width: 800px;
        height: 80px;
    }
    
    .calender .hightlight
    {
        font-weight: 600px;
    }
    
    .calender .days td:hover
    {
        background-color: #DEF;
    }
</style>
<body>
    <?= $navBar ?>
    <div class="container-fluid">    
        <div class="row">
            <?= $main ?>
        </div>
    </div>
    <?= $footer ?>
</body>
</html>