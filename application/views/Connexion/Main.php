<!------------------------------------------------------------------------------------------------------------------------------------------->
<!----------------------------------------------------   Formulaire php   ------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------->
                                            <div class="pt-5" style="min-height: 800px;">
<!------------------------------------------------------------------------------------------------------------------------------------------->

<!-- Connexion  -->
<?php var_dump($_SESSION);?>
<div class="mt-5 mx-auto w-50 h-50 p-5" style="box-shadow: 0px 0px 4px 0px #666;">
    <form method="POST" action="">
        <div class='form-group'>
            <label for="pseudo">Pseudo :</label>
            <input type="text" class="form-control" placeholder="Votre pseudo" id="pseudo" name="pseudo" />
        </div>
        <div class='form-group'>
            <label for="mdp">Mot de passe :</label>
            <input type="password" class="form-control" placeholder="Votre mot de passe" id="mdp" name="mdp" />
        </div>
        <div class="mt-5" style="text-align: center;">
            <input type="submit" value="Connexion" name="formconnexion" class="btn btn-primary">
        </div>
    </form>
</div>


<!------------------------------------------------------------------------------------------------------------------------------------------->
<!----------------------------------------------------   Partie alerte php   ---------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------->


<div class="mt-5 mx-auto w-50 h-50">
<?php if (isset($_SESSION['success'])) { ?> <!-- affichage du message de réussite --> 
<div class="alert alert-success"> <?= $_SESSION{'success'} ?></div>
<?php } ?>
<?= validation_errors('<div class="alert alert-danger">','</div>'); ?>  <!-- affichage des messages d'erreurs des champs vides -->
<?php if ($this->session->flashdata('error')) { ?> <!-- affichage des messages d'erreurs des champs invalide -->
    <div class="alert alert-danger"> <?= $this->session->flashdata('error') ?> </div>
<?php } ?>
</div>


<!----------------------------------------------------------------->
<!---------------------------   FIN   ----------------------------->
<!----------------------------------------------------------------->

<!------------------------------------------------------------------------------------------------------------------------------------------->
                                                            </div>
<!------------------------------------------------------------------------------------------------------------------------------------------->
