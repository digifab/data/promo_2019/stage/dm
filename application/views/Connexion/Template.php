<!-- Referez-vous à la page Actualite/Template pour les commentaire -->

<!DOCTYPE html>
<html>
<head>
    <?= $head ?>
    <title><?= $title ?></title>
</head>
<body>
    <?= $navBar ?>
    <div><?= $main ?></div>   
    <?= $footer ?>
</body>
</html>