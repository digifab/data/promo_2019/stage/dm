<!-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------   Un footer doit se composer au moins de:             ----------------------------------------------------------------------------------------
--------------------------------   Mentions légales                                    ----------------------------------------------------------------------------------------
--------------------------------   Carte du site web                                   ----------------------------------------------------------------------------------------
--------------------------------   Informations de contact                             ----------------------------------------------------------------------------------------
--------------------------------   Informations sur l’identification de l’entreprise   ----------------------------------------------------------------------------------------
--------------------------------   Liens vers les pages principales                    ----------------------------------------------------------------------------------------
--------------------------------   Liens vers le téléchargement de documents           ---------------------------------------           Proute proute           --------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>

<!-- Footer -->
<footer class="page-footer font-small unique-color-white"  style=" box-shadow: 0px -2px 6px -2px rgba(0,0,0,1); background: #333; color: white; width: 100%; position: relative; bottom: 0px;">

  <div style="background-color: #333;">
    <div class="container">

      <!-- Grid row-->
      <div class="row py-4 d-flex align-items-center">

      </div>
      <!-- Grid row-->

    </div>
  </div>

  <!-- Footer Links -->
  <div class="container text-center text-md-left mt-5">

    <!-- Grid row -->
    <div class="row mt-3">

      <!-- Grid column -->
      <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">

        <!-- Content -->
        <h6 class="text-uppercase font-weight-bold">Démons et Merveilles</h6>
        <hr style="height: 3px; width: 200px; background: #888; display: inline-block; ">
        <p>Ouverte à toutes et à tous, nous concevons le jeu de rôle comme bien plus qu'un hobby ou une lubie, c'est une activité qui rassemble, ludique, de création et d'interprétation.
C'est à chaque fois un moment unique de plaisir partagé.</p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

        <!-- Links -->
        <h6 class="text-uppercase font-weight-bold">Menu</h6>
        <hr style="height: 3px; width: 200px; background: #888; display: inline-block; ">
        <p>
          <a style="color: white;"  href="<?=site_url()?>">Acceuil</a>
        </p>
        <p>
          <a style="color: white;" href="<?site_url('Forum/')?>">Forum</a>
        </p>
        <p>
          <a style="color: white;" href="<?=site_url('Agenda/')?>">Agenda</a>
        </p>
        <p>
          <a style="color: white;" href="<?=site_url('Generateur/')?>">Jeux</a>
        </p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

        <!-- Links -->
        <h6 class="text-uppercase font-weight-bold">Liens Utiles</h6>
        <hr style="height: 3px; width: 200px; background: #888; display: inline-block; ">
        <p>
          <a style="color: white;" href="<?= site_url('Mention/')?>">Mention Légale</a>
        </p>
        <p>
          <a style="color: white;" href="#!">En savoir plus</a>
        </p>
        <p>
          <a style="color: white;" href="#!">nous contactez</a>
        </p>
        <p>
          <a style="color: white;" href="#!">Help</a>
        </p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

        <!-- Links -->
        <h6 style="color: white;" class="text-uppercase font-weight-bold">Contact</h6>
        <hr style="height: 3px; width: 200px; background: #888; display: inline-block; ">
        <p >
          <i class="fas fa-envelope mr-3"></i>organijeux@gmail.com</p>
        <p>
          <i class="fas fa-phone mr-3"></i>07 67 58 12 22</p>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">
    <a href="192.168.1.122/DM/"> <p style="color: white;" >© 2019 Copyright - Démons et Mérveilles, association de loi 1901 | Mentions légale fr</p></a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->