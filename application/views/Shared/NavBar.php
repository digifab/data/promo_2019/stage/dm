<!---------------------------------------------------------------------------------------------------------------------------->
<!---------------------------------------------------------------------------------------------------------------------------->
<!---------------------------------------------------------------------------------------------------------------------------->
<!---------------------------------------------   MENUS DE NAVIGATION   ------------------------------------------------------>
<!---------------------------------------------------------------------------------------------------------------------------->
<!---------------------------------------------------------------------------------------------------------------------------->



<nav class="navbar navbar-expand-lg sticky-top navbar-light" style=" background: #F7F7F7; box-shadow: 0px 0px 6px -2px rgba(0,0,0,1);">                          <!-- Barre de Navigation -->
    <a href="<?= site_url('') ?>"><img style=" text-align: center; width: 42px; height: 42px;" src="<?= base_url('assets/img/global/') ?>dm_D.png"></a>
   <a class="navbar-brand" href="<?= site_url('Actualite/')?>">Démons & Merveilles</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url('Forum/')?>">Forum</a>   <!-- site_url est une fonction qui permet d'accèder a la racine du site plus le chemin voulut-->
            </li>
            <li class="nav-item">
               <a class="nav-link" href="<?= site_url('Agenda/')?>">Agenda</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Jeux
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item text-center" href="<?= site_url('Generateur/')?>">Generateur</a>
                    <!--<div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?= site_url('Generateur/AjoutPersonnage')?>">Ajouter un personnage pour de futures generations</a>-->
                </div>  
            </li>
            <?php
            if(!isset($_SESSION['user_logged']))
            {
            }elseif($this->db->select("rang")->from("espace_membres")->where("pseudo", $_SESSION["pseudo"])->where("rang = 2")->get()->result())
            {?>
                <li class="nav-item"> 
                <a class="nav-link" href="<?= site_url('Panel/')?>">Administration</a>
                </li>
                <?php
            } 

            ?>
        </ul>
    </div>
    <!-- ICONE DU SITE -->    




<!---------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------   Gestion affichage lien vers espace membre   ----------------------------------------------------->
<!---------------------------------------------------------------------------------------------------------------------------------------------------------->


<?php
                    
    if(!isset($_SESSION['user_logged']))
    {   
    ?>
                        <a class="btn btn-primary my-2 my-sm-0 ml-1 mr-1" href="<?= site_url('Auth/connexion')?>">Connexion</a>
                        <a class="btn btn-primary my-2 my-sm-0 ml-1 mr-1" href="<?= site_url('Auth/inscription')?>">Inscription</a>
        
                    <?php 
                    }elseif(isset($_SESSION['user_logged'])) 
                    {
                    ?> 
                        
                        <div class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php echo $_SESSION['pseudo'];?>
            </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item text-center" href="<?= site_url('Utilisateur/profil')?>">Votre profil</a>
                                <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="<?= site_url('Auth/deconnexion')?>">Se deconnecter</a>
                        </div>  
                        </div>
                    <?php
                    }

        ?> 
        <!--   fin php   -->
<!---------------------------------------------------------------------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------   FIN   ------------------------------------------------------------------------------>
<!---------------------------------------------------------------------------------------------------------------------------------------------------------->


</nav>