<div class="py-5 text-center text-white h-100 align-items-center d-flex" style="background-image: linear-gradient(rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0.75)), url(&quot;https://static.pingendo.com/cover-bubble-dark.svg&quot;); background-position: center center, center center; background-size: cover, cover; background-repeat: repeat, repeat;" >
    <div class="container py-5">
      <div class="row">
        <div class="mx-auto col-lg-8 col-md-10">
          <h1 class="display-3 mb-4">Démons et merveilles</h1>
          <p class="lead mb-5">Vous ne connaissez pas du tout le Jeu de Rôle ? <strong>Parfait !</strong> Cette association est faite pour vous !<br>Vous n'avez plus jouer depuis une obscure nuit d'hiver et d'insouciance ?<strong> Parfait !</strong> Il faut pallier au manque !<br>Vous êtes Rôliste sans meneur, ou "pire", vous êtes curieux de goûter aux autres ?<strong> Parfait !</strong> Venez tâter de l'incongru et déguster de l'éphémère clameur d'oniriques réalités avec nous.<br>Et en plus, on est sympa.<br></p> <a href="#" class="btn btn-lg btn-primary mx-1">accéder au générateur</a>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5 bg-dark" style="">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h1 class="text-light">Petite histoire</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 p-3">
          <p class="text-light">Il y a de cela de nombreuses années, au siècle dernier pour être plus précis, notre histoire commence. A l'heure de ses lignes ceux par qui tout débute, sont des adultes épanouis, au chaud dans leurs pénates, inconscients peut-être, fiers quelque part, d'avoir créé de leurs jeux et de leur envie de se réunir une association qui perdure et s'ancre dans la vie au quotidien de ses membres<br><br>&nbsp;et de la Ville. Ces innocents étudiants, comme nous nous amuserons à les imaginer, s'amusent ensemble. Ils se retrouvent en bas de la rue, chez les parents, dans les locaux de l'université. Ils jouent à Rêve de Dragons, Cyberpunk, Ambre, et bien d 'autres jeux encore, s'imaginent et se décrivent combattre, par la lame et par l'esprit de valeureux adversaires, de terribles énigmes. Et puis on s'imagine un ailleurs sans jugement et sans interférence, sans restriction, un ailleurs chez soi.</p>
          <p class="text-light">A l'époque, faire du jeu de rôle est compliqué. En octobre de cette même année, une émission en fait son sujet phare, associant au satanisme la pratique du jeu, évoquant l'instabilité émotionnelle pouvant mener au suicide. Les gens prennent peur, parents, élus, voisins, chacun va avoir une réserve, s'interroger, sur ce qui ne paraissait jusqu'alors que naïfs jeux. Pourtant cela fait déjà une vingtaine d'année que le jeu de rôle a commencé à être publié et commercialisé.<br><br>&nbsp;Les ouvrages dédiés existent par centaines et les boutiques spécialisées voient peu à peu le jour. Créer une association de jeu de rôle est en soi à l'époque, une victoire. Son nom, ses premiers membres le choisissent en hommage à une nouvelle éponyme de l'auteur de science-fiction H.P Lovecraft.</p>
        </div>
        <div class="col-md-4 p-3">
          <p class="text-light">Sur 48h, dans un lieu dédié à l'événement, nous recevons des délégations d'autres associations, venues de la région mais également de toute la France, des badauds, des curieux, et des créateurs, auteurs et illustrateurs. Certains depuis, ont été édités et vivent de leur art. Des sites, blogs, forums, groupe et page web seront conçues pour être vus de tous. Des activités de découverte seront organisées&nbsp;<br><br>dans les écoles, collèges et centres aérés. Certains des membres, animateurs, professeurs, mettront leur connaissance et compétences à profit pour que l'initiation soit des plus adaptées au public visé. Durant plus de vingt-cinq années d'existence, nous investirons dans des ouvrages, des jeux, des biens matériels (d'aucun saurait vous narrer les années passées à voter pour l'achat d'un micro-onde) et dans des nouveaux locaux.</p>
          <p class="text-light">Le premier lieu de rassemblement sera Montivilliers, à la salle du quartier des Lombards. Avec les années, de nouveaux membres, et avec eux de nouveaux jeux, viennent changer les habitudes. On découvre et on se met à Warhammer, à Dark Earth. Aux étudiants du début, s'ajoutent de plus ou moins jeunes travailleurs, des lycéens. Chacun donne à l'association une part de son imagination, de sa bonne volonté, de son savoir<br>et Démons et Merveilles se nourrit de toutes ses différences. Se créera un journal, aujourd'hui disparu, le « DoudilMag », reprenant avec<br><br>humour, les déboires des personnages, les jeux de mots et les situations cocasses. De cette envie de se rencontrer, de découvrir les autres et de partager, naîtra une convention, « la Nuit du Jeu », qui perdure jusqu'à aujourd'hui, sous le nom de « Wanted, Nerd or Alive ».</p>
        </div>
        <div class="col-md-4 p-3">
          <p class="text-light">Avec le temps, les mentalités et les discours officiels évoluent. Des études sérieuses ont rendu leurs lettres de noblesse au jeu de rôle. Les sciences médicales ont découvert que la pratique de ce jeu est un excellent exercice dans la prévention et pour ralentir les symptômes de maladie dégénérative neurologique. De nombreux ouvrages de psychologie et de sciences du langage ont vanté les mérites d'un jeu qui met en œuvre l'expression&nbsp;<br><br>et l'empathie. Le rôle de l'association change. On fait appel à elle pour son savoir et les qualités intrinsèques du jeu de rôle, l'écoute et la parole, l'imagination, le travail émotionnel, l'échange. Aujourd'hui présente à la Fabrique Rouelles, Démons et Merveilles participe activement à la vie de la ville en prenant part aux fêtes de quartier et d'associations.</p>
          <p class="text-light">La disparité de nos membres, de 13 à plus de 60 ans, du collégien au docteur, en passant par tous les corps de métiers, fait notre force. Les nouvelles technologies et les modes ont influencées nos propres modes de fonctionnement. On nous trouve présents sur Facebook, et sur d'autres plateformes d'échanges numériques. Nos joueurs se sont également tournés vers des jeux tirés des univers de Manga, comme One Piece, d'influence japonaise, ou viking, au gré des séries, des réadaptations et&nbsp;<br><br>rééditions. Nous restons, comme aux toutes premières heures, ouverts à tous, gardant une place libre à chacune de nos tables. Nous rassemblons toutes les personnes qui souhaitent jouer, s'amuser et rêver avec nous, quelque soit leur origine, leur culture, leur histoire. Certains ont grandi, d'autres ont changé, mais ce qui anime l'association, à savoir Promouvoir le Jeu, reste au cœur de nos actions.</p>
        </div>
      </div>
    </div>
  </div>
  <!---------------------------------------------------------   FORMULAIRE DE SES MORTS   --------------------------------------------------------------------------->
  <div class="py-5">
    <div class="container-fluid px-5">
      <div class="row p-0 m-0">
        <div class="mx-auto text-center col-lg-6 p-0">
          <h1 class="mb-3">Contactez nous</h1>
        </div>
      </div>
      <div class="row">
        <div class="order-2 order-md-1 col-lg-6 p-5"><iframe width="100%" height="320" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10360.43626538166!2d0.1605971!3d49.5202166!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xdaf0fa19d51f99ba!2sAssociation%20D%C3%A9mons%20et%20Merveilles!5e0!3m2!1sfr!2sfr!4v1572438264910!5m2!1sfr!2sfr" frameborder="0" allowfullscreen=""></iframe></div>
        <div class="order-1 order-md-2 col-lg-6 p-5 px-5">
          <h2 class="mb-4">Formulaire</h2>
          <form>
            <div class="form-group"> <input type="text" class="form-control" id="form44" placeholder="Nom, prénom"> </div>
            <div class="form-group"> <input type="email" class="form-control" id="form45" placeholder="Email"> </div>
            <div class="form-group"> <textarea class="form-control" id="form46" rows="3" placeholder="Votre message..."></textarea> </div> <button type="submit" class="btn btn-primary">envoyez</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5 text-center bg-danger">
    <div class="container">
      <div class="row">
        <div class="mx-auto col-md-10">
          <p class="lead text-light"> Vous ne connaissez pas du tout le Jeu de Rôle ? Parfait ! Cette association est faite pour vous ! Vous n'avez plus jouer depuis une obscure nuit d'hiver et d'insouciance ? Parfait ! Il faut pallier au manque ! Vous êtes Rôliste sans meneur, ou "pire", vous êtes curieux de goûter aux autres ? Parfait ! Venez tâter de l'incongru et déguster de l'éphémère clameur d'oniriques réalités avec nous. Et en plus, on est sympa. </p>
        </div>
      </div>
    </div>
  </div>
