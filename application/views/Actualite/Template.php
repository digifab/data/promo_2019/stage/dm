<!-- Création de la structure de la page Actualité dans la qu'elle les differents parites de la page vont étre chargé, le main correspond à plus de 2/3 de la page -->

<!DOCTYPE html>
<html>
<head>
    <?= $head ?>
    <title><?= $title ?></title>
    <meta name="description" content="Découverte, Initiation et pratique des jeux de simulation.
Une bande de rôlistes, amis depuis longtemps, un jour, au siècle dernier, se réunirent et formèrent Démons et Merveilles.
Depuis, l'association a grandi, déménagé plusieurs fois, et est toujours bien vivante.
Ouverte à toutes et à tous, nous concevons le jeu de rôle comme bien plus qu'un hobby ou une lubie, c'est une activité qui rassemble, ludique, de création et d'interprétation.
C'est à chaque fois un moment unique de plaisir partagé.
Retrouvez-nous pour jouer, jeux de carte ou de plateaux, jeux de rôles, les vendredi soir, samedi et dimanche après-midi, à la Fabrique de Rouelles."/>
</head>
<body>
    <?= $navBar ?>
        <?= $main ?>
    <?= $footer ?>
</body>
</html>