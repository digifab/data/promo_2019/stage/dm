<!------------------------------------------------------------------------------------->
<!------------------------   CORP DE PAGE / BANNIERE + BOUTON   ----------------------->
<!------------------------------------------------------------------------------------->
        <div class="col-sm-3 mx-auto" style="top: 50%;">
            <!--<a class="btn-lg btn-block btn btn-primary btn-lg [b]btn-break[/b]" href="<?= site_url('../Generateur')?>">
            <p style="display: inline-block; margin: 0px; padding: 0px;">accéder au générateur</p>
            </a>-->
        </div>
        <img id="accueil_banner" class="img-fluid" src="<?= base_url('../assets/img/Accueil/') ?>banner.jpg" />
<!------------------------------------------------------------------------------------->
<!--------------------------------------   FIN   -------------------------------------->
<!------------------------------------------------------------------------------------->
<div class="text-center bg-dark text-light">
    <p>
        Vous ne connaissez pas du tout le Jeu de Rôle ? <strong>Parfait !</strong> Cette association est faite pour vous !<br/>Vous n'avez plus jouer depuis une obscure nuit d'hiver et d'insouciance ?<strong> Parfait !</strong> Il faut pallier au manque !<br/>Vous êtes Rôliste sans meneur, ou "pire", vous êtes curieux de goûter aux autres ?<strong> Parfait !</strong> Venez tâter de l'incongru et déguster de l'éphémère clameur d'oniriques réalités avec nous.<br/>Et en plus, on est sympa.<br/>
    </p>
    <hr/>
    <h3>Petite histoire</h3>
    <div class="row m-0">
        <div class="col-md-4">
            <p>
            Il y a de cela de nombreuses années, au siècle dernier pour être plus précis, notre histoire commence.<br/>A l'heure de ses lignes ceux par qui tout débute, sont des adultes épanouis, au chaud dans leurs pénates, inconscients peut-être, fiers quelque part, d'avoir créé de leurs jeux et de leur envie de se réunir une association qui perdure et s'ancre dans la vie au quotidien de ses membres et de la Ville.<br/>Ces innocents étudiants, comme nous nous amuserons à les imaginer, s'amusent ensemble. Ils se retrouvent en bas de la rue, chez les parents, dans les locaux de l'université. Ils jouent à Rêve de Dragons, Cyberpunk, Ambre, et bien d 'autres jeux encore, s'imaginent et se décrivent combattre, par la lame et par l'esprit de valeureux adversaires, de terribles énigmes. Et puis on s'imagine un ailleurs sans jugement et sans interférence, sans restriction, un ailleurs chez soi.
            </p>
        </div>

        <div class="col-md-4">
            <p>
            A l'époque, faire du jeu de rôle est compliqué. En octobre de cette même année, une émission en fait son sujet phare, associant au satanisme la pratique du jeu, évoquant l'instabilité émotionnelle pouvant mener au suicide. Les gens prennent peur, parents, élus, voisins, chacun va avoir une réserve, s'interroger, sur ce qui ne paraissait jusqu'alors que naïfs jeux. Pourtant cela fait déjà une vingtaine d'année que le jeu de rôle a commencé à être publié et commercialisé. Les ouvrages dédiés existent par centaines et les boutiques spécialisées voient peu à peu le jour.<br/>Créer une association de jeu de rôle est en soi à l'époque, une victoire. Son nom, ses premiers membres le choisissent en hommage à une nouvelle éponyme de l'auteur de science-fiction H.P Lovecraft.
            </p>
        </div>

        <div class="col-md-4">
            <p>
                Le premier lieu de rassemblement sera Montivilliers, à la salle du quartier des Lombards. Avec les années, de nouveaux membres, et avec eux de nouveaux jeux, viennent changer les habitudes. On découvre et on se met à Warhammer, à Dark Earth. Aux étudiants du début, s'ajoutent de plus ou moins jeunes travailleurs, des lycéens. Chacun donne à l'association une part de son imagination, de sa bonne volonté, de son savoir et Démons et Merveilles se nourrit de toutes ses différences. Se créera un journal, aujourd'hui disparu, le « DoudilMag », reprenant avec humour, les déboires des personnages, les jeux de mots et les situations cocasses.<br/>De cette envie de se rencontrer, de découvrir les autres et de partager, naîtra une convention, « la Nuit du Jeu », qui perdure jusqu'à aujourd'hui, sous le nom de « Wanted, Nerd or Alive ».
            </p>
        </div>

           <!-- <div class="col-md-4">
    
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fdetm76%2F&tabs=timeline&width=450&height=800&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" 
                width="480px" 
                height="800px" 
                id="fb-root"
                scrolling="no" 
                frameborder="0" 
                allowTransparency="true"
                allow="encrypted-media">
                </iframe>
            </div>-->
    </div>
    <hr/>
    <div class="row m-0">
    <div class="col-md-4">
            <p>
            Sur 48h, dans un lieu dédié à l'événement, nous recevons des délégations d'autres associations, venues de la région mais également de toute la France, des badauds, des curieux, et des créateurs, auteurs et illustrateurs.<br/>Certains depuis, ont été édités et vivent de leur art. Des sites, blogs, forums, groupe et page web seront conçues pour être vus de tous.<br/>Des activités de découverte seront organisées dans les écoles, collèges et centres aérés. Certains des membres, animateurs, professeurs, mettront leur connaissance et compétences à profit pour que l'initiation soit des plus adaptées au public visé.<br/>Durant plus de vingt-cinq années d'existence, nous investirons dans des ouvrages, des jeux, des biens matériels (d'aucun saurait vous narrer les années passées à voter pour l'achat d'un micro-onde) et dans des nouveaux locaux.
            </p>
        </div>

        <div class="col-md-4">
            <p>
            Avec le temps, les mentalités et les discours officiels évoluent. Des études sérieuses ont rendu leurs lettres de noblesse au jeu de rôle. Les sciences médicales ont découvert que la pratique de ce jeu est un excellent exercice dans la prévention et pour ralentir les symptômes de maladie dégénérative neurologique. De nombreux ouvrages de psychologie et de sciences du langage ont vanté les mérites d'un jeu qui met en œuvre l'expression et l'empathie. Le rôle de l'association change. On fait appel à elle pour son savoir et les qualités intrinsèques du jeu de rôle, l'écoute et la parole, l'imagination, le travail émotionnel, l'échange.<br/>Aujourd'hui présente à la Fabrique Rouelles, Démons et Merveilles participe activement à la vie de la ville en prenant part aux fêtes de quartier et d'associations.
            </p>
        </div>

        <div class="col-md-4">
            <p>
            La disparité de nos membres, de 13 à plus de 60 ans, du collégien au docteur, en passant par tous les corps de métiers, fait notre force. Les nouvelles technologies et les modes ont influencées nos propres modes de fonctionnement. On nous trouve présents sur Facebook, et sur d'autres plateformes d'échanges numériques. Nos joueurs se sont également tournés vers des jeux tirés des univers de Manga, comme One Piece, d'influence japonaise, ou viking, au gré des séries, des réadaptations et rééditions. Nous restons, comme aux toutes premières heures, ouverts à tous, gardant une place libre à chacune de nos tables. Nous rassemblons toutes les personnes qui souhaitent jouer, s'amuser et rêver avec nous, quelque soit leur origine, leur culture, leur histoire. Certains ont grandi, d'autres ont changé, mais ce qui anime l'association, à savoir Promouvoir le Jeu, reste au cœur de nos actions.
            </p>
        </div>
    </div>
</div>

<!---------------------------------------------------------   FORMULAIRE DE SES MORTS   --------------------------------------------------------------------------->


<div class="bg-danger">
    <div class="p-5 w-100 h-100">
      <div class="h-100">
        <div class="row h-100 w-50 p-5" >
            <div class="col-sm-2">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2590.108882981971!2d0.1584084158710411!3d49.52022006183139!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e02ff1af58f4bf%3A0xe59552629957564b!2s151%20Rue%20Ad%C3%A8le%20Robert%2C%2076610%20Le%20Havre!5e0!3m2!1sfr!2sfr!4v1568291980995!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="">
            </iframe>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5 bg-dark">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h1 class="text-light">Petite histoire</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 p-3">
          <p class="text-light">Il y a de cela de nombreuses années, au siècle dernier pour être plus précis, notre histoire commence. A l'heure de ses lignes ceux par qui tout débute, sont des adultes épanouis, au chaud dans leurs pénates, inconscients peut-être, fiers quelque part, d'avoir créé de leurs jeux et de leur envie de se réunir une association qui perdure et s'ancre dans la vie au quotidien de ses membres et de la Ville. Ces innocents étudiants, comme nous nous amuserons à les imaginer, s'amusent ensemble. Ils se retrouvent en bas de la rue, chez les parents, dans les locaux de l'université. Ils jouent à Rêve de Dragons, Cyberpunk, Ambre, et bien d 'autres jeux encore, s'imaginent et se décrivent combattre, par la lame et par l'esprit de valeureux adversaires, de terribles énigmes. Et puis on s'imagine un ailleurs sans jugement et sans interférence, sans restriction, un ailleurs chez soi.</p>
          <p class="text-light">Sur 48h, dans un lieu dédié à l'événement, nous recevons des délégations d'autres associations, venues de la région mais également de toute la France, des badauds, des curieux, et des créateurs, auteurs et illustrateurs. Certains depuis, ont été édités et vivent de leur art. Des sites, blogs, forums, groupe et page web seront conçues pour être vus de tous. Des activités de découverte seront organisées dans les écoles, collèges et centres aérés. Certains des membres, animateurs, professeurs, mettront leur connaissance et compétences à profit pour que l'initiation soit des plus adaptées au public visé. Durant plus de vingt-cinq années d'existence, nous investirons dans des ouvrages, des jeux, des biens matériels (d'aucun saurait vous narrer les années passées à voter pour l'achat d'un micro-onde) et dans des nouveaux locaux.</p>
          <p class="text-light">Avec le temps, les mentalités et les discours officiels évoluent. Des études sérieuses ont rendu leurs lettres de noblesse au jeu de rôle. Les sciences médicales ont découvert que la pratique de ce jeu est un excellent exercice dans la prévention et pour ralentir les symptômes de maladie dégénérative neurologique. De nombreux ouvrages de psychologie et de sciences du langage ont vanté les mérites d'un jeu qui met en œuvre l'expression et l'empathie. Le rôle de l'association change. On fait appel à elle pour son savoir et les qualités intrinsèques du jeu de rôle, l'écoute et la parole, l'imagination, le travail émotionnel, l'échange. Aujourd'hui présente à la Fabrique Rouelles, Démons et Merveilles participe activement à la vie de la ville en prenant part aux fêtes de quartier et d'associations.</p>
        </div>
        <div class="col-md-6 p-3">
          <p class="text-light">A l'époque, faire du jeu de rôle est compliqué. En octobre de cette même année, une émission en fait son sujet phare, associant au satanisme la pratique du jeu, évoquant l'instabilité émotionnelle pouvant mener au suicide. Les gens prennent peur, parents, élus, voisins, chacun va avoir une réserve, s'interroger, sur ce qui ne paraissait jusqu'alors que naïfs jeux. Pourtant cela fait déjà une vingtaine d'année que le jeu de rôle a commencé à être publié et commercialisé. Les ouvrages dédiés existent par centaines et les boutiques spécialisées voient peu à peu le jour. Créer une association de jeu de rôle est en soi à l'époque, une victoire. Son nom, ses premiers membres le choisissent en hommage à une nouvelle éponyme de l'auteur de science-fiction H.P Lovecraft.</p>
          <p class="text-light">Le premier lieu de rassemblement sera Montivilliers, à la salle du quartier des Lombards. Avec les années, de nouveaux membres, et avec eux de nouveaux jeux, viennent changer les habitudes. On découvre et on se met à Warhammer, à Dark Earth. Aux étudiants du début, s'ajoutent de plus ou moins jeunes travailleurs, des lycéens. Chacun donne à l'association une part de son imagination, de sa bonne volonté, de son savoir et Démons et Merveilles se nourrit de toutes ses différences. Se créera un journal, aujourd'hui disparu, le « DoudilMag », reprenant avec humour, les déboires des personnages, les jeux de mots et les situations cocasses. De cette envie de se rencontrer, de découvrir les autres et de partager, naîtra une convention, « la Nuit du Jeu », qui perdure jusqu'à aujourd'hui, sous le nom de « Wanted, Nerd or Alive ».</p>
          <p class="text-light">La disparité de nos membres, de 13 à plus de 60 ans, du collégien au docteur, en passant par tous les corps de métiers, fait notre force. Les nouvelles technologies et les modes ont influencées nos propres modes de fonctionnement. On nous trouve présents sur Facebook, et sur d'autres plateformes d'échanges numériques. Nos joueurs se sont également tournés vers des jeux tirés des univers de Manga, comme One Piece, d'influence japonaise, ou viking, au gré des séries, des réadaptations et rééditions. Nous restons, comme aux toutes premières heures, ouverts à tous, gardant une place libre à chacune de nos tables. Nous rassemblons toutes les personnes qui souhaitent jouer, s'amuser et rêver avec nous, quelque soit leur origine, leur culture, leur histoire. Certains ont grandi, d'autres ont changé, mais ce qui anime l'association, à savoir Promouvoir le Jeu, reste au cœur de nos actions.</p>
        </div>
      </div>
    </div>
  </div>
  <!---------------------------------------------------------   FORMULAIRE DE SES MORTS   --------------------------------------------------------------------------->
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="mx-auto text-center col-lg-6">
          <h1 class="mb-3 pt-2 pb-4">Contactez nous</h1>
        </div>
      </div>
      <div class="row">
        <div class="p-0 order-2 order-md-1 col-lg-6"> <iframe width="100%" height="350" src="https://maps.google.com/maps?hl=en&amp;q=New%20York&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" scrolling="no" frameborder="0"></iframe> </div>
        <div class="px-4 order-1 order-md-2 col-lg-6">
          <h2 class="mb-4">Formulaire</h2>
          <form>	
            <div class="form-group"> <input type="text" class="form-control" id="form44" placeholder="Nom, prénom"> </div>
            <div class="form-group"> <input type="email" class="form-control" id="form45" placeholder="Email"> </div>
            <div class="form-group"> <textarea class="form-control" id="form46" rows="3" placeholder="Votre message..."></textarea> </div> <button type="submit" class="btn btn-primary">envoyez</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5 text-center bg-danger">
    <div class="container">
      <div class="row">
        <div class="mx-auto col-md-7">
          <p class="lead text-light"> Vous ne connaissez pas du tout le Jeu de Rôle ? Parfait ! Cette association est faite pour vous ! Vous n'avez plus jouer depuis une obscure nuit d'hiver et d'insouciance ? Parfait ! Il faut pallier au manque ! Vous êtes Rôliste sans meneur, ou "pire", vous êtes curieux de goûter aux autres ? Parfait ! Venez tâter de l'incongru et déguster de l'éphémère clameur d'oniriques réalités avec nous. Et en plus, on est sympa. </p>
        </div>
      </div>
    </div>
  </div>
