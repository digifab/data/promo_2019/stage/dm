<!-- Referez-vous à la page Actualite/Template pour les commentaire -->
<!DOCTYPE html>
<html>
<head>
    <?= $head ?>
    <title><?= $title ?></title>
</head>
<body>
    <?= $navBar ?>
        <div class="text-center">
            <?= $caroussel ?>
            <div class="text-center">
                <?= $choixUnivers ?>
            </div>  
        </div>
    <?= $footer ?>
</body>
</html>