<div class="container">
<?php
//Création de tableau de donnée via la base de données 
    $pageUnivers=array(); 
    $icone=array();
    $nomUnivers=array();
    foreach($records as $r)
    {
        
        array_push($pageUnivers ,($r->nomUnivers)); // création du chemin d'acceès vers la page de l'univers via son nom
        array_push($icone,base_url('assets/img/iconeUnivers/').($r->nomUnivers).".jpg"); // acces à l'icone représentant l'univers
    }

/********************************************************************************************************************************************************************************************
****************************************************************** fonction qui enleve tout les caractere spéciaux **************************************************************************
********************************************************************************************************************************************************************************************/

    function normalize ($string) 
{
    $table = array(
        'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
        'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
        'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
        'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
        'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
        'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', "'"=>"" , ' '=> ''
    );
    return strtr($string, $table);
}
    
?>
<!-- ************************************************************************************************************************************************************************************ -->
<!-- *********************************************************************** Création du code du caroussel ****************************************************************************** -->
<!-- ************************************************************************************************************************************************************************************ -->

<!-- Création d'une ancre pour créer une les flèches de défilement -->
<a class="carousel-control-prev bg-danger" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
            


<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">

        <?php 
            $i=0;
            while ($i < count($pageUnivers)-1 )
            {
                if( $i== 0)
                { 
        ?>
                    <div class="carousel-item active">
                        <div  methode="POST" class="row" id="univers_Select">
                            <?php 
                                while( $i < 4)
                                {
                            ?>
                                        
                                        <div class="col-3" >
                                            <div class="univers text-center" id="boutonUnivers">
                                                <a href="<?= site_url("Generateur/index/". normalize($pageUnivers[$i]))?>" > 
                                                    <img id="iconeUnivers" src="<?= $icone[$i] ?>"  />
                                                    <br/> 
                                                    <?= $pageUnivers[$i]?>
                                                </a>
                                            </div>
                                        </div>
                            <?php $i=$i+1; 
                                }    
                            ?>
                                        
                                   
                        </div>
                    </div>
        <?php 
                }
                if($i!=0 and $i < count($pageUnivers))
                {
                    $j=0;
        ?>
                    <div class="carousel-item">
                    <div  methode="POST" class="row" id="univers_Select">
                        <?php 
                            for( $j=0; $j<4;$j++)
                            {
                                if($i >= count($pageUnivers))
                                {
                                    $j=4;
                                    continue;
                                }
                                else
                                {
                        ?>      
                                    
                                    <div class="col-3" >
                                        <div class="univers text-center" id="boutonUnivers">
                                            <a href="<?= site_url("Generateur/index/".normalize($pageUnivers[$i]))?>" > 
                                                <img id="iconeUnivers" src="<?= $icone[$i] ?>"  /> 
                                                <br/> 
                                                <?= $pageUnivers[$i]?>
                                            </a>
                                        </div>
                                    </div>
                        <?php   }
								if($j>=4){}
								else
								{
									$i=$i+1;
								}

							}
                        ?>
                                    
                               
                    </div>
                </div>
        <?php
                }

            } 
        ?>
    </div>
        
        
    </div>
</div>
<a class="carousel-control-next bg-danger" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
</a>
<hr/>

