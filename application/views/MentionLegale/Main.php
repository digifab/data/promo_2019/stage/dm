<div class="row mx-auto w-50 mt-5 mb-5 ml-0 mr-0">
<div class="mx-auto">
<h3>Mentions légales</h3>
</div>

<div class="mx-auto">
<p style="text-align: center;">En vigueur au 28/09/2019</p>

<p>
Conformément aux dispositions des Articles 6-III et 19 de la Loi n°2004-575 du 21 juin 2004 pour la Confiance dans l’économie numérique, dite L.C.E.N., il est porté à la connaissance des Utilisateurs du site http://demonsetmerveilles.org les présentes mentions légales.
La connexion et la navigation sur le site <a href="http://demonsetmerveilles.org">demonsetmerveille</a> par l’Utilisateur implique acceptation intégrale et sans réserve des présentes mentions légales.
Ces dernières sont accessibles sur le site à la rubrique « Mentions légales ».</p><br />
</div>

<div class="mx-auto">
<h5>ARTICLE 1 : L’éditeur</h5>
</div>

<div class="mx-auto">
<p>L’édition et la direction de la publication du site <a href="http://demonsetmerveilles.org">demonsetmerveilles</a> est assurée par l’association Démons et Merveilles dont le siège social est Chez M. De Colombel, 32 rue Amiral Courbet 76600 Le Havre, ayant pour numéro de téléphone 0767581222 et l’adresse mail organijeux@gmail.com.
Le directeur de publication de Démons et Merveilles est le Président de l’association : Chatillon Jean-François, domicilié Chez M. Faure 36 rue Eugène Duromea 76620 Le Havre, dont le numéro de téléphone est 0619242287, et l'adresse e-mail jfchatillon@yahoo.fr. 
</p>
</div>

<div class="mx-auto">
<h5>ARTICLE 2 : L’hébergeur</h5>
</div>

<div class="mx-auto">
<p>L'hébergeur du site <a href="http://demonsetmerveilles.org">demonsetmerveilles</a> est l’association Démons et Merveilles dont le siège social est Chez M. De Colombel, 32 rue Amiral Courbet 76600 Le Havre, ayant pour numéro de téléphone 0767581222 et l’adresse mail organijeux@gmail.com.
.</p><br />
</div>

<div class="mx-auto">
<h5>ARTICLE 3 : Accès au site</h5>
</div>

<div class="mx-auto">
<p>
Le site est accessible par tout endroit, 7j/7, 24h/24 sauf cas de force majeure, interruption programmée ou non et pouvant découlant d’une nécessité de maintenance.
En cas de modification, interruption ou suspension des services le site <a href="http://demonsetmerveilles.org">demonsetmerveilles</a> ne saurait être tenu responsable.</p><br />
</div>

<div class="mx-auto">
<h5>ARTICLE 4 : Collecte des données</h5>
</div>

<div class="mx-auto">
<p>
Le site est exempté de déclaration à la Commission Nationale Informatique et Libertés (CNIL) dans la mesure où il ne collecte aucune donnée concernant les utilisateurs.
</p>
</div>

<div class="mx-auto">
<h5>ARTICLE 5 : Cookies</h5>
</div>

<div class="mx-auto">
<p>
L’Utilisateur est informé que lors de ses visites sur le site, un cookie peut s’installer automatiquement sur son logiciel de navigation.<br />
En naviguant sur le site, il les accepte.<br />
Un cookie est un élément qui ne permet pas d’identifier l’Utilisateur mais sert à enregistrer des informations relatives à la navigation de celui-ci sur le site Internet. L’Utilisateur pourra désactiver ce cookie par l’intermédiaire des paramètres figurant au sein de son logiciel de navigation.
 </p>
</div>

<div class="mx-auto">
<h5>ARTICLE 6 : Propriété intellectuelle</h5><br />
</div>

<div class="mx-auto">
<p>
Toute utilisation, reproduction, diffusion, commercialisation, modification de toute ou partie du site <a href="http://demonsetmerveilles.org">demonsetmerveilles</a>, sans autorisation de l’Editeur est prohibée et pourra entraînée des actions et poursuites judiciaires telles que notamment prévues par le Code de la propriété intellectuelle et le Code civil.
</p>
</div>



</div>