<?php


/************************************************************************************************************************************************/
/****************************************************   UNIVERS / nom dossier / lien img   ******************************************************/
/************************************************************************************************************************************************/


class modelDemonsMerveilles extends CI_Model  // création de la class modelGenerateur qui est l'enfant de la classe CI_Model
{
    public function nomUnivers()   //création de la fonction publique dataGen qui retourne la table gen_univers
    {
        $this->db->order_by('nomUnivers','ASC');
        $this->db->from('gen_univers');
        return $this->db->get()->result();     /*on récupère la table gen_univers
                                                 de la variable tableau qui contient
                                                 la connexion a la basse de donnée 
                                                 (application/config/database.php)
                                               */
    }


/************************************************************************************************************************************************/
/*****************************************************************   Forum   ********************************************************************/
/************************************************************************************************************************************************/



    public function categorie() // retourne la table f_categorie
    {
       return $this->db->get('f_categories')->result();
    }

    public function sous_categorie($categorie) //retourne la table f_souscategorie en fonction de leur categorie parent
    {
		$options=array('categorieParent'=>$categorie);
        return $this->db->get_where('f_souscategories',$options)->result();

    }

    public function nbr_topic($sousCategorieParent) 
	{
		$option=array(
			'sousCategorieParent'=>$sousCategorieParent
		);
		return $this->db->get_where('f_topics',$option)->result();
	}



    public function dernier_topic() //recherche les idTopics dans leur ordre de création
    {
        $this->db->order_by('idTopic','ASC');
        $this->db->from('f_topics');
        return $this->db->get()->result();
    }


/************************************************************************************************************************************************/
/****************************************************   Transformation caractère Univers   ******************************************************/
/************************************************************************************************************************************************/


/*    public function normalize ($string) 
    {
    $table = array(
        'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
        'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
        'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
        'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
        'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
        'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', "'"=>"" , ' '=> ''
    );
    return strtr($string, $table);
    }
    public $prefs;

*/


/************************************************************************************************************************************************/
/**************************************************************   AGENDA   **********************************************************************/
/************************************************************************************************************************************************/


    public function __construct()   // configuration du format d'affichage  de l'agenda 
    {
        $this->prefs = array(
			'start_day'    => 'monday',
            'month_type'   => 'long',
            'day_type'     => 'short',
			'show_next_prev'  => TRUE,
            'next_prev_url'   => base_url().'Agenda/index');
        $this->prefs['template'] = '

        {table_open}
			<table border="0" cellpadding="0" cellspacing="0" class="calender">{/table_open}
        {heading_row_start}<tr>{/heading_row_start}
        {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
        {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}
        {heading_row_end}</tr>{/heading_row_end}
        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td>{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}
        {cal_row_start}<tr class="days">{/cal_row_start}
        {cal_cell_start}<td>{/cal_cell_start}
        {cal_cell_start_today}<td>{/cal_cell_start_today}
        {cal_cell_start_other}<td class="other-month">{/cal_cell_start_other}
        {cal_cell_content}
        	<div class="day_num">{day}</div>
        	<div class="content">{content}</div>
        {/cal_cell_content}
        {cal_cell_content_today}
        <div class="">
        	<div class="day_num highlight">{day}</div>
        	<div class="content">{content}</div>
    	</div>
    	{/cal_cell_content_today}
        {cal_cell_no_content}{day}{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}
        {cal_cell_blank}&nbsp;{/cal_cell_blank}
        {cal_cell_other}{day}{/cal_cel_other}
        {cal_cell_end}</td>{/cal_cell_end}
        {cal_cell_end_today}</td>{/cal_cell_end_today}
        {cal_cell_end_other}</td>{/cal_cell_end_other}
        {cal_row_end}</tr>{/cal_row_end}
        {table_close}</table>{/table_close}
		';
        
    }

    public function getcalender($year , $month)
	{
		$this->load->library('calendar',$this->prefs); // Load calender library
		$data = $this->get_calender_data($year,$month);
		return $this->calendar->generate($year , $month , $data);
    }
    
	public function get_calender_data($year , $month) // affiche les evenement dans le calendrier
	{
		$query = $this->db->select('dateEvent,hourEvent,pseudo,content')->from('calendar')->join('espace_membres','calendar.idMembre = espace_membres.idMembre')->like('dateEvent',"$year-$month")->order_by('hourEvent','ASC')->get();
		$cal_data = array();
        foreach ($query->result() as $row)
        {
            $calendar_date = date("Y-m-j", strtotime($row->dateEvent)); // to remove leading zero from day format
            if(isset($cal_data[substr($calendar_date, 8,2)])) //si il peut ajouter dans dans la variable alors il le concataine sinon il l'ajout
            {
                $cal_data[substr($calendar_date, 8,2)] .= '<br/>'.$row->content.'<br/>'.$row->hourEvent.'<br/>'.$row->pseudo;
            }
            else
            {
                $cal_data[substr($calendar_date, 8,2)] = $row->content.'<br/>'.$row->hourEvent.'<br/>'.$row->pseudo;
            }
		}
		return $cal_data;
    }
    
	public function add_calendar_data($data , $dateEvent, $hourEvent) //Insertion des données de l'evenement réer dans la BDD
	{
		$this->db->insert('calendar',array(
			'dateEvent'	=> $dateEvent,
            'content'	=> $data,
            'hourEvent' => $hourEvent
		));
	}
}


/************************************************************************************************************************************************/
/************************************************************************************************************************************************/
/************************************************************************************************************************************************/