<?php

class espace_membre
{
	function inscription()
	{
	connect();
	
		if(isset($_POST['forminscription']))
		{
			$pseudo = ($_POST['pseudo']);
			$mail = ($_POST['mail']);
			$mail2 = ($_POST['mail2']);
			$mdp = ($_POST['mdp']);
			$mdp2 = ($_POST['mdp2']);

			if(!empty($_POST['pseudo']) AND !empty($_POST['mail']) AND !empty($_POST['mail2']) AND !empty($_POST['mdp']) AND !empty($_POST['mdp2']))
			{
				$pseudolength = strlen($pseudo);
				if($pseudolength <= 255)
				{
					if($mail == $mail2)
					{
						if(filter_var($mail, FILTER_VALIDATE_EMAIL))
						{
							$reqmail = $bdd->prepare("SELECT * FROM espace_membres WHERE email = ? ");
							$reqmail->execute(array($mail));
							$mailexist = $reqmail->rowCount();
							if($mailexist == 0)
							{
								if($mdp == $mdp2)
								{
									$insertmbr = $bdd->prepare("INSERT INTO espace_membres(pseudo, mdp, email, nom, prenom) VALUES(:pseudo, :mdp, :email, '', '')");
									$insertmbr->execute(array('pseudo' => $pseudo, 'mdp' => $mdp, 'email' => $mail));
									
									$erreur = "Votre compte a bien été créé, connectez-vous ! ";
								}
								else
								{
									$erreur = "Vos mots de passes ne correspondent pas !";
								}
							}
							else
							{
								$erreur = "Adresse mail déjà utilisée !";
							}
						}
						else
						{
							$erreur = "Votre adresse mail n'est pas valide !";
						}
					}
					else
					{
						$erreur = "Vos adresses mail ne correspondent pas !";
					}
				}
				else
				{
					$erreur = "Votre pseudo ne doit pas dépasser 255 caractères !";
				}
			}
			else
			{
				$erreur = "Tous les champs doivent être complétés !";
			}
		}
	}

	function connexion()
	{
		connect();

		if(isset($_POST['formconnexion']))
		{
			$mailconnect = htmlspecialchars($_POST['mailconnect']);
			$mdpconnect = sha1($_POST['mdpconnect']);
			if(!empty($mailconnect) AND !empty($mdpconnect))
			{
				$requser = $bdd->prepare("SELECT * FROM espace_membres WHERE pseudo = ? AND mdp = ?");
				$requser->execute(array($mailconnect, $mdpconnect));
				$userexist = $requser->rowCount();
				if($userexist == 1)
				{
					$userinfo = $requser->fetch();
					$_SESSION['id'] = $userinfo['id'];
					$_SESSION['pseudo'] = $userinfo['pseudo'];
					$_SESSION['mail'] = $userinfo['mail'];
					header("Location: profil.php?id=".$_SESSION['id']);
				}
				else
				{
					$erreur = "Mauvais mail ou mot de passe !";
				}
			}
			else
			{
				$erreur = "Tous les champs doivent être complétés !";
			}
		}
	}

	function deconnexion()
	{
	    session_start();
    	if isset($_SESSION['id'])
    	{ 
        $_SESSION = array();
        session_destroy(); 
        header('Location: actualite.php');
    	}else{
       		header('Location: profil.php');
    	}
	}
}
?>